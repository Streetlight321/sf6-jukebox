# Version 1.25.1.0

# Requirements
* Reframework                               : https://www.nexusmods.com/streetfighter6/mods/73
* Microsoft .NET 6.0 Framework              : https://dotnet.microsoft.com/en-us/download/dotnet/6.0
* Microsoft .NET 6.0 Desktop Runtime        : https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-desktop-6.0.21-windows-x64-installer
* Microsoft Visual C++ 2022 Redistributable : https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170


# First install, basic usage
* Make sure you have all requirements installed (see section above).
* Copy files included in this zip file into your .\Street Fighter 6\reframework\ folder.
* Copy your custom tracks in .\Street Fighter 6\reframework\plugins\CustomTracks folder. They can be organized in sub folders.
* Start the game, turn the game music all the way down in the audio menu.
* Your tracks should automatically play during fights.

# Playlist Manager
The playlist manager (located in the PlaylistManager folder coming with the mod) allows you to customize what tracks are played in different parts of the game. It also allows you to customize multiple SF6Jukebox settings.
## Settings
The settings tab is where you can customize global settings for the mod.
### SF6Jukebox Enabled
Enable or disable the mod.
### Override menu's tracks
Allows you to play custom tracks for game menus.
### Overlay enabled
Shows an overlay informing you about the track being played.
### Open
Opens the 'Random tracks folder' in the explorer.
### Random tracks folder
Folder in which SF6Jukebox will look for mp3s to play randomly when the "Play random tracks" option is enabled.
### Default Volume
Volume at which custom tracks will be played. Adjusted values will only be taken into account the next time you start the game.
### Playlist priority
What playlist to use at a beginning of a fight.
* Stage: Plays the current stage playlist.
* Player 1: Play the playlist associated with whovever Player 1 is playing.
* Player 2: Play the playlist associated with whovever Player 2 is playing.
* Player's character: Play the playlist associated with whovever you are playing (regardless of which side you're on).
* Opponent's character: Play the playlist associated with whovever your opponent is playing (regardless of which side he's on).
* Random: Picks any of those randomly.
### Shortcuts
You can use those shortcuts to control certain aspect of the playback. Currently, playing the next or previous track in the playlist, or adjusting the volume live. When you adjust the volume in game, that volume level will be saved across game sessions. You can redefine those shortcut bindings to any key you want by simply clicking once on the associated button, then any key you would like to use on your keyboard.
### Save
The save button at the bottom of the screen should be used to save any change you've made in the Playlist Manager.
## Menus / Stages / Misc
You will find in this section menus supporting custom track playback. The right section of the screen will be referred as the playlist (associated with the selected menu on the left).
### Play Mode
* Repeat: plays the entire playlist, then starts over. 
* Once: plays the playlist just once.
* SingleOnce: plays one random track then stops.
* SingleRepeat: plays one random track and repeats that same track.
### Enabled (Menus only)
Tick this option to have this playlist playing when you enter the associated menu. If left disabled, SF6Jukebox will leave whatever track was already playing, playing. You would use this option when you want the same track you set in menu A to keep playing while navigating to menu B to C then D. If the playlist for menu C is enabled, the track that menu A started will stop when entering menu C. This option is off by default meaning no track will play from menus until you enable it.
### Fade-out duration
Allows you to adjust how long the associated playlist tracks should fade-out for when stopped.
### Play random tracks
If enabled, the associated playlist will play tracks from the 'Random playlist folder' instead of the ones specified bellow.
### Add track
Opens an mp3 file(s) picker to add track to the playlist.
### Remove selected tracks
Remove seleted tracks.
### Play (on the track line)
Plays the associated track, for preview purposes.

# Upgrading to a new version
## Clean upgrade
* Make a backup of your "CustomTracks" folder in case you had any mp3s in there.
* Delete your "plugins", "PlaylistManager", and README.md file.
* Extract the new version inside the reframework folder.

## If you want to keep your existing playlists
* Make a backup of your "CustomTracks" folder in case you had any mp3s in there.
* Make a backup of you /plugins/settings.xml file.
* Delete your "plugins", "PlaylistManager", and README.md file.
* Extract the new version inside the reframework folder.
* Restore your backed up settings.xml file inside the plugins folder.
**This is still very experimental and might cause the mod to malfunction. If this happens, go with the "Clean upgrade" option.**

# Issues / Limitations
* When playing online, the original tracks might still play on top of your custom ones, to go around this, you can set the BGM volume to 0 in the game's menu.
* All screens are not yet customizable with tracks, but it's coming.

# Support
* Discord: https://discord.gg/7shjQFRNDF
* Donation: https://ko-fi.com/adysoft

# Credits
* Author: Yann Alet (https://gitlab.com/yann.alet)
* PlaylistManager icon: Freepik https://www.flaticon.com/free-icon/playlist_907786

## Dependencies:
* REFramework: praydog (https://github.com/praydog)
* NAudio: https://github.com/naudio/NAudio
* TinyXML: https://github.com/leethomason/tinyxml2
* Taglib: https://taglib.org