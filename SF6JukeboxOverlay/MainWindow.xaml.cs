﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using SF6JukeboxOverlay.Service;
using SF6JukeboxOverlay.GlobalEvents;
using SF6JukeboxOverlay.ViewModel;

namespace SF6JukeboxOverlay
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string strClassName, string strWindowName);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

        FileLogger FileLogger { get; set; }
        Thread ProcessWatcherThread { get; set; }
        DispatcherTimer OutroTimer { get; set; }
        Storyboard IntroStoryboard { get; set; }
        Storyboard OutroStoryboard { get; set; }

        public MainWindow(FileLogger logger)
        {
            InitializeComponent();
            FileLogger = logger;
            DataContext = new MainWindowViewModel(new OverlayClientService(logger), logger);
            EventAggregator.Subscribe<ShowOverlayEvent>(OnShowOverlayEvent);
            IntroStoryboard = Resources["ShowSB"] as Storyboard;
            OutroStoryboard = Resources["HideSB"] as Storyboard;
            OutroStoryboard.Completed += OutroStoryboard_Completed;
            ProcessWatcherThread = new Thread(ProcessWatcher);
            ProcessWatcherThread.Start();
            OutroTimer = new DispatcherTimer();
            OutroTimer.Interval = TimeSpan.FromSeconds(5);
            OutroTimer.Tick += OutroTimer_Tick;
            PlaceOffscreen();
        }

        private void OnShowOverlayEvent(ShowOverlayEvent @event)
        {
            ShowOverlay();
        }

        private void OutroStoryboard_Completed(object? sender, EventArgs e)
        {
            OutroTimer.Stop();
            PlaceOffscreen();
        }

        private void OutroTimer_Tick(object? sender, EventArgs e)
        {
            PlayOutro();
        }

        void PlayIntro()
        {
            IntroStoryboard.Begin();            
        }

        void PlayOutro()
        {
            FileLogger.Log("PlayOutro");
            OutroStoryboard.Begin();
        }

        void PlaceOffscreen()
        {
            FileLogger.Log("PlaceOffscreen");
            Top = -1000;
            Opacity = 0;
        }

        void ShowOverlay()
        {
            FileLogger.Log("ShowOverlay");
            Center();
            PlayIntro();            
            OutroTimer.Start();
        }

        private void ProcessWatcher(object? obj)
        {
            bool keepWatching = true;
            while (keepWatching)
            {
                if(Process.GetProcessesByName("StreetFighter6").Length == 0)
                {
                    if (Application.Current != null)
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            Close();
                        }));
                    }
                    keepWatching = false;
                }

                Thread.Sleep(100);
            }
        }

        private void Center()
        {
            FileLogger.Log("Center");
            Process[] processes = Process.GetProcessesByName("StreetFighter6");
            if (processes.Length > 0)
            {
                Process sf6Process = processes[0];
                IntPtr ptr = sf6Process.MainWindowHandle;
                Rect rect = new Rect();
                GetWindowRect(ptr, ref rect);
                var windowWidth = Application.Current.MainWindow.ActualWidth;
                var windowHeight = Application.Current.MainWindow.ActualHeight;
                var sf6Width = rect.Right - rect.Left;
                var mainWindow = Application.Current.MainWindow;
                mainWindow.Topmost = true;
                mainWindow.Top = rect.Bottom - (windowHeight / 2) - 80;
                mainWindow.Left = rect.Left + (sf6Width / 2) - (windowWidth / 2);
            }
            else
            {
                Top = 300;
                Left = 300;
            }
        }
    }
}
