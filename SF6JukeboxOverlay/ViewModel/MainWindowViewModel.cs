﻿using SF6JukeboxOverlay.GlobalEvents;
using SF6JukeboxOverlay.Service;
using System.Linq;
using System.Windows;
using System;
using System.Windows.Media.Imaging;
using MediaPlayerLib.Network.Contract;
using System.IO;
using MediaPlayerLib.Network;

namespace SF6JukeboxOverlay.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel(OverlayClientService overlayClientService, ILogger logger)
        {
            OverlayClientService = overlayClientService;
            Logger = logger;
            OverlayClientService.OnTrackUpdate += OverlayClientService_OnTrackUpdate;
            OverlayClientService.OnShowOverlay += OverlayClientService_OnShowOverlay;
            OverlayClientService.OnNetworkError += OverlayClientService_OnNetworkError;
            OverlayClientService.Start();
        }

        private float progress;
        private string artist;
        private string album;
        private string title;
        private BitmapImage cover;
        private Visibility coverVisibility;

        private OverlayClientService OverlayClientService { get; }
        private string LastPlayedTrack { get; set; }
        private ILogger Logger { get; }

        public string Title
        {
            get => title;
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }
        public string Album
        {
            get => album;
            set
            {
                album = value;
                OnPropertyChanged();
            }
        }
        public string Artist
        {
            get => artist;
            set
            {
                artist = value;
                OnPropertyChanged();
            }
        }

        public float Progress 
        {
            get => progress; 
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage Cover
        {
            get => cover;
            set
            {
                cover = value;
                OnPropertyChanged();
            }
        }

        public Visibility CoverVisibility
        {
            get => coverVisibility;
            set
            {
                coverVisibility = value;
                OnPropertyChanged();
            }
        }


        private void OverlayClientService_OnNetworkError(object? sender, string e)
        {
            OverlayClientService.Stop();
            Logger.Log(e);
        }

        private void OverlayClientService_OnShowOverlay(object? sender, System.EventArgs e)
        {
            EventAggregator.Publish(new ShowOverlayEvent());
        }

        private void OverlayClientService_OnTrackUpdate(object? sender, Model.TrackUpdate e)
        {
            Logger.Log("UpdateUI");
            if (LastPlayedTrack != e.Track)
            {
                TagLib.File file = TagLib.File.Create(e.Track);
                Progress = e.Progress * 100;
                Title = String.IsNullOrEmpty(file.Tag.Title) ? Path.GetFileNameWithoutExtension(e.Track) : file.Tag.Title;
                Album = String.IsNullOrEmpty(file.Tag.Album) ? "Unknown" : file.Tag.Album; ;
                if (file.Tag.AlbumArtists.Any())
                {
                    Artist = file.Tag.AlbumArtists[0];
                }

                if (file.Tag.Pictures.Any())
                {
                    CoverVisibility = Visibility.Visible;
                    Cover = GetBitmapImageFromByteArray(file.Tag.Pictures.First().Data.Data);
                }
                else
                {
                    CoverVisibility = Visibility.Collapsed;
                }
                LastPlayedTrack = e.Track;
            }

            Progress = e.Progress * 100;
        }

        private BitmapImage? GetBitmapImageFromByteArray(byte[] data)
        {
            try
            {
                using (var mStream = new MemoryStream())
                {
                    byte[] pData = data;
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    mStream.Flush();
                    mStream.Position = 0;
                    BitmapImage bitmapimage = new BitmapImage();
                    bitmapimage.BeginInit();
                    bitmapimage.StreamSource = mStream;
                    bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapimage.EndInit();
                    mStream.Dispose();
                    return bitmapimage;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return null;
            }
        }
    }
}
