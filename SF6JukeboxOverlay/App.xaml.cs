﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace SF6JukeboxOverlay
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        string LogFilePath
        {
            get
            {
                var executingDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
                return Path.Combine(executingDir.FullName, @"logs.txt"); ;
            }
        }

        FileLogger FileLogger { get; }

        public App()
        {
            FileLogger = new FileLogger(LogFilePath);
            ShutdownMode = ShutdownMode.OnLastWindowClose;
            Startup += App_Startup;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            MainWindow = new MainWindow(FileLogger);
            MainWindow.Show();
        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            FileLogger.Log(e.Exception.ToString());            
        }
    }
}
