﻿namespace SF6JukeboxOverlay.Model
{
    public class TrackUpdate
    {
        public string Track { get; set; }
        public float Progress { get; set; }
    }
}
