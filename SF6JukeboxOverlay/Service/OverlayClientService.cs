﻿using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.Network.Message;
using SF6JukeboxOverlay.Model;
using System;
using System.Windows;

namespace SF6JukeboxOverlay.Service
{
    public class OverlayClientService
    {
        public event EventHandler<TrackUpdate> OnTrackUpdate;
        public event EventHandler OnShowOverlay;
        public event EventHandler<string> OnNetworkError;

        public OverlayClientService(ILogger logger)
        {
            OverlayClient = new OverlayClient(1212, logger);
            OverlayClient.OnTrackUpdateMessage += OverlayClient_OnTrackUpdateMessage; ;
            OverlayClient.OnShowOverlayMessage += OverlayClient_OnShowOverlayMessage; ;
            OverlayClient.OnNetworkError += OverlayClient_OnNetworkError; ;            
        }

        OverlayClient OverlayClient { get; }

        public void Start()
        {
            OverlayClient.Start();
        }

        public void Stop()
        {
            OverlayClient.Stop();
        }

        private void OverlayClient_OnNetworkError(object? sender, string e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnNetworkError?.Invoke(sender, e);
            }));
        }

        private void OverlayClient_OnShowOverlayMessage(object? sender, ShowOverlayMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnShowOverlay?.Invoke(this, EventArgs.Empty);
            }));
        }

        private void OverlayClient_OnTrackUpdateMessage(object? sender, TrackUpdateMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnTrackUpdate?.Invoke(this, new TrackUpdate { Track = e.Track, Progress = e.Progress });
            }));
        }
    }
}
