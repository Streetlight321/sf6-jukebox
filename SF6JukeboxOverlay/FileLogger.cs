﻿using MediaPlayerLib.Network.Contract;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace SF6JukeboxOverlay
{
    public class FileLogger : ILogger
    {
        Semaphore Locker = new Semaphore(1, 1);

        public FileLogger(string filePath)
        {
            FilePath = filePath;
            if(File.Exists(FilePath)) 
            { 
                File.Delete(FilePath);
            }
        }

        public string FilePath { get; }

        public void Log(string message)
        {
            Locker.WaitOne();
            message = $"[{DateTime.Now}] : {message}\r\n";
            using(var fs = new FileStream(FilePath, FileMode.Append))
            {
                fs.Write(Encoding.UTF8.GetBytes(message));
                fs.Flush();
            }
            Locker.Release();
        }
    }
}
