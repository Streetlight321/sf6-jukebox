#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

	__declspec(dllexport) typedef struct CLogEventArgs
	{
		CLogEventArgs()
			: Message(nullptr) { }

		char* Message;
	} CLogEventArgs;

	__declspec(dllexport) typedef struct CVolumeChangedArg
	{
		CVolumeChangedArg()
			: Volume(0.0f) { }

		float Volume;
	} CVolumeChangedArg;

	__declspec(dllexport) void playCharacterTrack(int characterId);
	__declspec(dllexport) void playStageTrack(int stageId);
	__declspec(dllexport) void playFlowSceneTrack(int flowSceneId);
	__declspec(dllexport) void playCustomScreenTrack(int customScreenId);
	__declspec(dllexport) void setPosition(int position);
	__declspec(dllexport) void stop();
	__declspec(dllexport) void increaseVolume();
	__declspec(dllexport) void decreaseVolume();
	__declspec(dllexport) void playNextTrack();
	__declspec(dllexport) void playPreviousTrack();
	__declspec(dllexport) void resetStagePlaylist();
	__declspec(dllexport) void resetFlowScenePlaylist();
	__declspec(dllexport) void resetCustomScreenPlaylist();
	__declspec(dllexport) void resetCharacterPlaylist();
	__declspec(dllexport) void queueInNextTrack();
	__declspec(dllexport) void saveSettings();
	__declspec(dllexport) void showOverlay();

	__declspec(dllexport) void subscribe_onLog(void(*funcPtr)(CLogEventArgs));

#ifdef __cplusplus
}
#endif
