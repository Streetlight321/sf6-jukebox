#include "pch.h"
#include "GameStateAPI.h"
#include "PlayerAPI.h"
#include "RefAPI.h"
#include "Logger.h"
#include "FlowScene.h"
#include "PerfTracker.h"
#include "include/reframework/API.hpp"
#include <string>
#include <vector>
#include <random>

typedef reframework::API::ManagedObject RefManagedObject;

GameStateAPI* GameStateAPI::instance = nullptr;

GameStateAPI::GameStateAPI()
{
    playerAPI = PlayerAPI::GetInstance();
    settings = Settings::GetInstance();
    lastP1CharacterState = new Character(Fight::PlayerType::PlayerType_Unknown, 0);
    lastP2CharacterState = new Character(Fight::PlayerType::PlayerType_Unknown, 0);
}

GameStateAPI* GameStateAPI::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new GameStateAPI();
	}

	return instance;
}

void GameStateAPI::Update()
{
    PERF_TRACKING("GameStateAPI::Update", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    HandleFlowScene();
    HandleShortcuts();    
}

bool GameStateAPI::WasPreviousFlowSceneFight()
{
    PERF_TRACKING("GameStateAPI::WasPreviousFlowSceneFight", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    return LastFlowScene == Game::FlowScene::eWorldTourBattle || LastFlowScene == Game::FlowScene::eBattleMain;
}

bool GameStateAPI::IsFlowSceneEnabled(Game::FlowScene flowScene)
{
    PERF_TRACKING("GameStateAPI::IsFlowSceneEnabled", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    return Settings::GetInstance()->FlowScenes.count(flowScene)
        && Settings::GetInstance()->FlowScenes[flowScene].IsEnabled();
}

void GameStateAPI::PlayForFlowScene(Game::FlowScene flowScene)
{
    PERF_TRACKING("GameStateAPI::PlayForFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    // Little hack to avoid creating a variation of the character creation screen in the PlaylistManager.
    if (flowScene == Game::FlowScene::eWorldTourCharacterCreateShop)
    {
        flowScene = Game::FlowScene::eBattleHubCharacterCreateShop;
    }

    if (WasPreviousFlowSceneFight() || (IsFlowSceneEnabled(flowScene) && playerAPI->GetCurrentPlayingFlowScene() != flowScene))
    {
        if (playerAPI->IsPlayingTrack())
        {
            Logger::GetInstance()->LogInfo(std::string("PlayForFlowScene - ") + std::to_string(flowScene));
            playerAPI->stopInternal();
        }

        if (IsFlowSceneEnabled(flowScene))
        {
            Logger::GetInstance()->LogInfo(std::string("PlayForFlowScene - ") + std::to_string(flowScene));
            playerAPI->playFlowSceneTrack(flowScene);
        }
    }
}

void GameStateAPI::HandleFlowScene()
{
    PERF_TRACKING("GameStateAPI::HandleFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    bool flowSceneUpdated = false;
    auto flowScene = GetFlowScene();
    if (LastFlowScene != flowScene)
    {
        Logger::GetInstance()->LogInfo("Flowscene updated from " + std::to_string(LastFlowScene) + " to " + std::to_string(flowScene));
        LastFlowScene = flowScene;
        flowSceneUpdated = true;
    }

    HandleSpectateMode(flowScene);
    MuteBGM(flowScene);

    if (!settings->IsOverrideMenusTracksEnabled() && OutOfFightFlowScenes.count(flowScene) && playerAPI->IsPlayingTrack())
    {
        // If at any point we were in a fight (or spectating), 
        // and we jump somewhere else when menus tracks override is disabled,
        // we should stop playing everything.
        playerAPI->stopInternal();
    }

    // Weird bug with eSpectateAssetReload which must be ignored to avoid tracks playing simultaneously.
    if (flowScene == Game::FlowScene::eWorldTourBattle || flowScene == Game::FlowScene::eBattleMain || flowScene == Game::FlowScene::eSpectateAssetReload)
    {
        LastFlowScene = flowScene;
        HandleFightState();        
    }
    else if(flowSceneUpdated && settings->IsOverrideMenusTracksEnabled())
    {
        PlayForFlowScene(flowScene);
    }
}

bool GameStateAPI::IsWorldTourFight()
{
    PERF_TRACKING("GameStateAPI::IsWorldTourFight", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto flowScene = GetFlowScene();
    auto fightState = GetFightState();
    if (flowScene == Game::FlowScene::eWorldTourCity
        && fightState > Fight::State::StageInit)
    {
        return true;
    }

    return false;
}

void GameStateAPI::HandleFightState()
{
    PERF_TRACKING("GameStateAPI::HandleFightState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto state = GetFightState();
    auto gameMode = GetGameMode();
    if (state != LastFightState)
    {
        Logger::GetInstance()->LogInfo("Fight state updated from : " + std::to_string(LastFightState) + " to " + std::to_string(state));
        LastFightState = state;
    }    

    switch (state)
    {
        case Fight::State::StageInit:
            IsCharacterStateOverrideDetected = false;
            CurrentRandomPlaylistPriority = std::nullopt;
            break;
        case Fight::State::RoundInit:
        case Fight::State::Appear:
        case Fight::State::Ready:
        case Fight::State::Now:
            if (gameMode == Game::Mode::WorldTour || !HandleCharacterState())
            {
                if (!playerAPI->IsPlayingStageTrack() || playerAPI->IsPlayingCharacterOverride())
                {
                    Logger::GetInstance()->LogInfo(std::string("HandleFightState - state: " + std::to_string(state)));
                    playerAPI->playStageTrack(GetStage());
                }
            }            
            break;

        case Fight::State::WinWait:
            if (gameMode == Game::Mode::Practice)
            {
                // Cut character override after a reset in practice mode.
                IsCharacterStateOverrideDetected = false;
            }
            break;
            
        case Fight::State::Finish:
            IsCharacterStateOverrideDetected = false;      
            CurrentRandomPlaylistPriority = std::nullopt;

            // If we were fighting in World Tour, we want a different track for the next fight.
            if (gameMode == Game::Mode::WorldTour)
            {
                Logger::GetInstance()->LogInfo(std::string("HandleFightState - World Tour Detected - state: " + std::to_string(state)));
                playerAPI->queueInNextTrack();
            }
            else if(playerAPI->GetCurrentPlayingCustomScreen() != Game::Screen::ResultScreen)
            {
                if (settings->IsOverrideMenusTracksEnabled())
                {
                    Logger::GetInstance()->LogInfo(std::string("HandleFightState - state: " + std::to_string(state)));
                    playerAPI->playCustomScreenTrack(Game::ResultScreen);
                }
                else if (playerAPI->IsPlayingTrack())
                {
                    Logger::GetInstance()->LogInfo(std::string("HandleFightState - state: " + std::to_string(state)));
                    playerAPI->stopInternal();
                }
            }
            
            break;
        default:
            break;
    }       
}

bool GameStateAPI::HandleCharacterState()
{
    PERF_TRACKING("GameStateAPI::HandleCharacterState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    Character p1Character = GetCharacter(1);
    Character p2Character = GetCharacter(2);
    if (!p1Character.Equals(*lastP1CharacterState))
    {
        lastP1CharacterState = &p1Character;
    }

    if (!p2Character.Equals(*lastP2CharacterState))
    {
        lastP2CharacterState = &p2Character;
    }    

    if (!CurrentRandomPlaylistPriority)
    {
        CurrentRandomPlaylistPriority = { Settings::GetInstance()->FightPlaylistPriority };
        if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Random)
        {
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> distr((int)Settings::PlaylistPriority::Stage, (int)Settings::PlaylistPriority::Opponent);
            CurrentRandomPlaylistPriority = { static_cast<Settings::PlaylistPriority>(distr(gen)) };
        }
    }

    if (p1Character.ShouldOverrideTrack() && p2Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->IsOverrideMenusTracksEnabled())
        {
            if (!playerAPI->IsPlaying(p1Character.GetBothTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p1Character.GetBothTrackOverrideScreenId());
            }
        }
        else 
        {
            if (playerAPI->IsPlayingTrack())
            {
                playerAPI->stopInternal();
                return IsCharacterStateOverrideDetected;
            }
        }
    }
    else if (p1Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->IsOverrideMenusTracksEnabled())
        {
            if (!playerAPI->IsPlaying(p1Character.GetSimpleTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p1Character.GetSimpleTrackOverrideScreenId());
            }            
        }
        else
        {
            if (playerAPI->IsPlayingTrack())
            {
                playerAPI->stopInternal();
                return IsCharacterStateOverrideDetected;
            }
        }
    }
    else if (p2Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->IsOverrideMenusTracksEnabled())
        {
            if (!playerAPI->IsPlaying(p2Character.GetSimpleTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p2Character.GetSimpleTrackOverrideScreenId());
            }            
        }
        else
        {
            if (playerAPI->IsPlayingTrack())
            {
                playerAPI->stopInternal();
                return IsCharacterStateOverrideDetected;
            }
        }
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Stage)
    {
        return IsCharacterStateOverrideDetected;
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::P1)
    {
        if (!playerAPI->IsPlaying(p1Character.Type))
        {
            playerAPI->playCharacterTrack(p1Character.Type);
        }
        return true;
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::P2)
    {
        if (!playerAPI->IsPlaying(p2Character.Type))
        {
            playerAPI->playCharacterTrack(p2Character.Type);
        }
        return true;
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Player)
    {
        BattleDescription battleDescription = GetBattleDescription();
        if (!playerAPI->IsPlaying(battleDescription.GetPlayerCharacter()))
        {
            playerAPI->playCharacterTrack(battleDescription.GetPlayerCharacter());
        }
        return true;
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Opponent)
    {
        BattleDescription battleDescription = GetBattleDescription();
        if (!playerAPI->IsPlaying(battleDescription.GetOpponentCharacter()))
        {
            playerAPI->playCharacterTrack(battleDescription.GetOpponentCharacter());
        }
        return true;
    }

    return IsCharacterStateOverrideDetected;
}

void GameStateAPI::HandleShortcuts()
{
    PERF_TRACKING("GameStateAPI::HandleShortcuts", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto& api = reframework::API::get();

    if (GetAsyncKeyState(settings->Shortcuts.VolumeUp) == 0 && WasVolumeUpPressed)
    {
        playerAPI->saveSettings();
        WasVolumeUpPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeDown) == 0 && WasVolumeDownPressed)
    {
        playerAPI->saveSettings();
        WasVolumeDownPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.NextTrack) & 0x8000)
    {
        WasNextTrackPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.PreviousTrack) & 0x8000)
    {
        WasPreviousTrackPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.Restart) & 0x8000)
    {
        WasRestartPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.ShowOverlay) & 0x8000)
    {
        WasShowOverlayPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeUp) & 0x8000)
    {
        playerAPI->increaseVolume();
        WasVolumeUpPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeDown) & 0x8000)
    {
        playerAPI->decreaseVolume();
        WasVolumeDownPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.NextTrack) == 0 && WasNextTrackPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->playNextTrack();
        }

        WasNextTrackPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.PreviousTrack) == 0 && WasPreviousTrackPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->playPreviousTrack();
        }

        WasPreviousTrackPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.Restart) == 0 && WasRestartPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->setPosition(0);
        }

        WasRestartPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.ShowOverlay) == 0 && WasShowOverlayPressed)
    {
        if (Settings::GetInstance()->IsOverlayEnabled() && playerAPI->IsPlayingTrack())
        {
            playerAPI->showOverlay();
        }

        WasShowOverlayPressed = false;
    }
}

Fight::State GameStateAPI::GetFightState()
{
    PERF_TRACKING("GameStateAPI::GetFightState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto fightState = RefAPI::TryGetStaticFieldValue<uint8_t>("gBattle", { "Game", "fight_st" });
    if (fightState.has_value())
    {
        return static_cast<Fight::State>(fightState.value());
    }
    
    return Fight::State::StageInit;
}

Fight::Stage GameStateAPI::GetStage()
{
    PERF_TRACKING("GameStateAPI::GetStage", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    // If we're in world tour mode, there are no stage ids;
    if (GetGameMode() == Game::Mode::WorldTour)
    {
        return Fight::Stage::WorldTour;
    }

    auto stageId = RefAPI::TryCallManagedSingletonFunction<int>("app.battle.stage.ESGimmickManager", "get_StageId");

    switch (stageId.value())
    {
        case Fight::Stage::BarmaleySteelworks:
            return Fight::Stage::BarmaleySteelworks;
        case Fight::Stage::BathersBeach:
            return Fight::Stage::BathersBeach;
        case Fight::Stage::CarrierByronTaylor:
            return Fight::Stage::CarrierByronTaylor;
        case Fight::Stage::Colosseo:
            return Fight::Stage::Colosseo;
        case Fight::Stage::DhalsimerTemple:
            return Fight::Stage::DhalsimerTemple;
        case Fight::Stage::FeteForaine:
            return Fight::Stage::FeteForaine;
        case Fight::Stage::GenbuTemple:
            return Fight::Stage::GenbuTemple;
        case Fight::Stage::KingStreet:
            return Fight::Stage::KingStreet;
        case Fight::Stage::MetroCityDowntown:
            return Fight::Stage::MetroCityDowntown;
        case Fight::Stage::OldTownMarket:
            return Fight::Stage::OldTownMarket;
        case Fight::Stage::RangersHut:
            return Fight::Stage::RangersHut;
        case Fight::Stage::SuvalhalArena:
            return Fight::Stage::SuvalhalArena;
        case Fight::Stage::TheMachoRing:
            return Fight::Stage::TheMachoRing;
        case Fight::Stage::ThunderfootSettlement:
            return Fight::Stage::ThunderfootSettlement;
        case Fight::Stage::TianHongYuan:
            return Fight::Stage::TianHongYuan;
        case Fight::Stage::TrainingRoom:
            return Fight::Stage::TrainingRoom;
        default:
            return Fight::Stage::Stage_Unkown;
    }
}

Game::Mode GameStateAPI::GetGameMode()
{
    PERF_TRACKING("GameStateAPI::GetGameMode", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto gameMode = RefAPI::TryGetStaticFieldValue<int>("gBattle", { "Config", "_GameMode" });
    if (gameMode)
    {
        switch (*gameMode)
        {
        case Game::Mode::Arcade:
            return Game::Mode::Arcade;
        case Game::Mode::BattleHub:
            return Game::Mode::BattleHub;
        case Game::Mode::Casual:
            return Game::Mode::Casual;
        case Game::Mode::Practice:
            return Game::Mode::Practice;
        case Game::Mode::SpecialMatch:
            return Game::Mode::SpecialMatch;
        case Game::Mode::Versus:
            return Game::Mode::Versus;
        case Game::Mode::Spectate:
            return Game::Mode::Spectate;
        case Game::Mode::WorldTour:
        case Game::Mode::WorldTourTraining:
            return Game::Mode::WorldTour;
        default:
            return Game::Mode::Mode_Unknown;
        }
    }
    else
    {
        return Game::Mode::Mode_Unknown;
    }
}

Character GameStateAPI::GetCharacter(int playerIndex)
{
    PERF_TRACKING("GameStateAPI::GetCharacter", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    Character character(Fight::PlayerType::PlayerType_Unknown, 0);
    auto fightState = GetFightState();
    
    if (fightState > Fight::State::StageInit && fightState <= Fight::State::Now)
    {        
        auto playerType = RefAPI::TryGetStaticFieldValue<RefManagedObject*>("gBattle", { "Player", "mPlayerType"});
        if (*playerType)
        {            
            auto v = RefAPI::TryGetArrayItem<RefManagedObject*>(*playerType, playerIndex-1);
            if (v)
            {
                auto pType = RefAPI::TryGetFieldValue<uint32_t>(*v, { "mValue" });
                if (pType)
                {
                    character = Character(static_cast<Fight::PlayerType>(*pType), 0);
                }
            }
        }
    }
    
    if(fightState == Fight::State::Now)
    {
        auto result = RefAPI::TryCallStaticFieldFunction<reframework::API::ManagedObject*>("gBattle", "Player", playerIndex == 1 ? "get1P" : "get2P");
        if(result)
        {
            reframework::API::ManagedObject* player = *result;
            auto fightStyle = RefAPI::TryGetFieldValue<int>(player, { "mCheckInfo", "FightStyle" });

            if (fightStyle)
            {
                character.FightStyle = *fightStyle;
            }
        }
    }

    return character;
}

BattleDescription GameStateAPI::GetBattleDescription()
{
    PERF_TRACKING("GameStateAPI::GetBattleDescription", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    BattleDescription battleDescription(Fight::PlayerType::PlayerType_Unknown, Fight::PlayerType::PlayerType_Unknown);
    auto m_flows = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.bFlowManager", { "m_flows" });
    if (m_flows)
    {        
        auto item = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*m_flows, 1);
        if (item)
        {
            auto fighterDescs = RefAPI::TryGetFieldValue<RefManagedObject*>(item.value(), {"m_battle_core", "_FighterDescs"});
            if (fighterDescs)
            {
                auto fighterDesc0 = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*fighterDescs, 0);
                auto fighterDesc1 = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*fighterDescs, 1);
                if (fighterDesc0 && fighterDesc1)
                {
                    auto fighterId0 = RefAPI::TryGetFieldValue<int>(*fighterDesc0, { "FighterId" });
                    auto fighterId1 = RefAPI::TryGetFieldValue<int>(*fighterDesc1, { "FighterId" });
                    auto padId0 = RefAPI::TryGetFieldValue<int>(*fighterDesc0, { "PadId" });
                    auto padId1 = RefAPI::TryGetFieldValue<int>(*fighterDesc1, { "PadId" });
                    if (fighterId0 && fighterId1 && padId0 && padId1)
                    {
                        if (*padId0 == 0)
                        {
                            battleDescription = BattleDescription(static_cast<Fight::PlayerType>(*fighterId0), static_cast<Fight::PlayerType>(*fighterId1));
                        }
                        else
                        {
                            battleDescription = BattleDescription(static_cast<Fight::PlayerType>(*fighterId1), static_cast<Fight::PlayerType>(*fighterId0));
                        }
                    }
                }                
            }
        }        
    }

    return battleDescription;
}

Game::FlowScene GameStateAPI::GetFlowScene()
{
    PERF_TRACKING("GameStateAPI::GetFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto value = RefAPI::TryCallManagedSingletonFunction<uint32_t>("app.bFlowManager", "get_MainFlowID");
    if (!value)
    {
        Logger::GetInstance()->LogError("Could not get flow ID");
        return Game::FlowScene::FlowScene_Unknown;
    }
    else
    {
        return static_cast<Game::FlowScene>(*value);
    }
}

// Only works for stages.
void GameStateAPI::MuteBGM(Game::FlowScene flowScene)
{
    PERF_TRACKING("GameStateAPI::MuteBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    if (IsSpectating)
    {
        // Spectating causes a black screen at the end of a fight, so I use the flowMap to kill the soundtrack.
        // I wish it was that clean in other screens...
        MuteSpectateBGM();
    }
    else
    {
        MuteInteractiveFightBGM();
    }
}

void GameStateAPI::MuteSpectateBGM()
{
    PERF_TRACKING("GameStateAPI::MuteSpectateBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto flowMap = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.bFlowManager", { "m_flow_work", "_FlowMap" });
    if (flowMap)
    {
        std::string typeName = (*flowMap)->get_type_definition()->get_full_name();
        if (typeName == "app.battle.SpectateFlowMap")
        {
            RefAPI::TryCallFunction<void*>(*flowMap, "StopBGM", {});
        }
    }
}

void GameStateAPI::MuteInteractiveFightBGM()
{
    PERF_TRACKING("GameStateAPI::MuteInteractiveFightBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    if (_LoadedPrefabDic)
    {
        auto count = RefAPI::TryGetFieldValue<int>(*_LoadedPrefabDic, { "_count" });
        if (count && *count > 0)
        {
            RefAPI::TryCallFunction<void*>(*_LoadedPrefabDic, "Clear", {});
            Logger::GetInstance()->LogInfo("BGM muted.");
        }
    }
}

void GameStateAPI::HandleSpectateMode(Game::FlowScene flowScene)
{
    PERF_TRACKING("GameStateAPI::HandleSpectateMode", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    if (flowScene == Game::FlowScene::eSpectateSetup)
    {
        IsSpectating = true;
    }

    if (OutOfFightFlowScenes.count(flowScene))
    {
        IsSpectating = false;
    }
}