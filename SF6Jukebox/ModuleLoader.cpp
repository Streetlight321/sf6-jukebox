#include "pch.h"
#include "ModuleLoader.h"

#include <Windows.h>
#include <filesystem>

ModuleLoader* ModuleLoader::instance = nullptr;

void Dummy()
{
}

ModuleLoader::ModuleLoader()
{
	handle = LoadLibrary(GetBridgeDllPath().c_str());
}

ModuleLoader* ModuleLoader::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new ModuleLoader();
	}

	return instance;
}

std::string ModuleLoader::GetModulePath()
{
    char path[256];
    HMODULE hm = NULL;

    if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR)&Dummy, &hm) == 0)
    {
        int ret = GetLastError();
        fprintf(stderr, "GetModuleHandle failed, error = %d\n", ret);
        // Return or however you want to handle an error.
    }
    if (GetModuleFileName(hm, path, sizeof(path)) == 0)
    {
        int ret = GetLastError();
        fprintf(stderr, "GetModuleFileName failed, error = %d\n", ret);
        // Return or however you want to handle an error.
    }
    std::string dllPath = std::string(path);
    std::string pluginParentPath = std::filesystem::path{ dllPath }.parent_path().string();
    return pluginParentPath;
}

std::string ModuleLoader::GetBridgeDllPath()
{
    auto path = std::filesystem::path{ GetModulePath() } / std::filesystem::path{ "MediaPlayerLibBridge.dll" };
    return path.string();
}
