#pragma once

#include <string>
#include <optional>
#include "include/reframework/API.hpp"
#include "Logger.h"

namespace RefAPI
{
	template <class T>
	std::optional<T> TryCallFunction(reframework::API::ManagedObject* instance, std::string functionName, std::vector<void*> functionParams)
	{
		auto fieldTypeDefinition = instance->get_type_definition();
		if (fieldTypeDefinition)
		{
			auto method = fieldTypeDefinition->find_method(functionName);
			if (method)
			{
				auto r = method->invoke(instance, functionParams);
				if (!r.exception_thrown)
				{
					if (r.ptr)
					{
						return { reinterpret_cast<T>(r.ptr) };
					}
					else //todo: Very dangerous as is, must support other cases, use std::is_same
					{
						return { reinterpret_cast<T>(r.dword) };
					}
				}
				else
				{
					LogError("Failed to invoke: " + functionName);
				}
			}
			else
			{
				LogError("Failed to find method: " + functionName);
			}
		}

		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryGetCollectionItemAt(reframework::API::ManagedObject* instance, int index)
	{
		return RefAPI::TryCallFunction<T>(instance, "get_Item", { (void*)index });
	}

	template <class T>
	std::optional<T> TryCallManagedSingletonFunction(std::string singletonName, std::string methodName)
	{
		auto& api = reframework::API::get();
		auto singleton = api->get_managed_singleton(singletonName);
		if (singleton != nullptr)
		{
			auto result = singleton->invoke(methodName, {});
			if (result.exception_thrown)
			{
				LogError("Exception when attempting to call: " + methodName);
			}
			else
			{
				return { static_cast<T>(result.dword) };
			}
		}
		else
		{
			LogError("Failed to get singleton: " + singletonName);
		}

		return std::nullopt;
	}	

	template <class T>
	std::optional<T> TryGetFieldValue(const reframework::API::ManagedObject* owner, std::vector<std::string> fieldPath)
	{
		if (fieldPath.size() == 1)
		{
			auto resultPtr = owner->get_field<T>(fieldPath[0]);
			if (resultPtr)
			{
				T r = static_cast<T>(*resultPtr);
				return { r };
			}
			else
			{
				LogError("Failed get_field for: " + fieldPath[0]);
				return std::nullopt;
			}
		}
		else
		{
			auto o = owner->get_field<reframework::API::ManagedObject*>(fieldPath[0]);
			fieldPath.erase(fieldPath.begin());
			return TryGetFieldValue<T>(*o, fieldPath);
		}		
	}

	template <class T>
	std::optional<T> TryGetManagedSingletonField(std::string singletonName, std::vector<std::string> fieldPath)
	{
		auto& api = reframework::API::get();
		auto singleton = api->get_managed_singleton(singletonName);
		return TryGetFieldValue<T>(singleton, fieldPath);
	}

	template <class T>
	std::optional<T> TryGetStaticFieldValue(std::string className, std::vector<std::string> fieldPath)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		const auto classType = tdb->find_type(className);
		const auto field = classType->find_field(fieldPath[0]);
		const auto fieldValue = field->get_data<reframework::API::ManagedObject*>();
		if (fieldValue)
		{
			fieldPath.erase(fieldPath.begin());
			return TryGetFieldValue<T>(fieldValue, fieldPath);
		}
		else
		{
			return std::nullopt;
		}
	}

	template <class T>
	std::optional<T> TryCallStaticFieldFunction(std::string className, std::string fieldName, std::string methodName, std::vector<void*> args)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		const auto classType = tdb->find_type(className);
		const auto field = classType->find_field(fieldName);
		const auto fieldValue = field->get_data<reframework::API::ManagedObject*>();
		if (fieldValue)
		{
			auto fieldTypeDefinition = fieldValue->get_type_definition();
			if (fieldTypeDefinition)
			{
				auto method = fieldTypeDefinition->find_method(methodName);
				if (method)
				{
					auto r = method->invoke(fieldValue, args);					
					if constexpr (std::is_same_v<T, reframework::API::ManagedObject*>)
					{
						return { static_cast<T>(r.ptr) };
					}
					else if constexpr (std::is_same_v<T, int>)
					{
						return { static_cast<T>(r.dword) };
					}
					else
					{
						LogError("Failed to invoke: " + methodName);
					}
				}
				else
				{
					LogError("Failed to find method: " + methodName);
				}
			}
			else
			{
				LogError("Failed to get field type definition for field: " + fieldName);
			}
		}
		else
		{
			LogError("Failed to get field value : " + fieldName);
		}

		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryCallStaticFieldFunction(std::string className, std::string fieldName, std::string methodName)
	{
		return TryCallStaticFieldFunction<T>(className, fieldName, methodName, {});
	}

	template <class T>
	std::optional<T> TryGetArrayItem(reframework::API::ManagedObject* array, int itemIndex)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		auto arrayDefinition = tdb->find_type("System.Array");
		auto getValueMethod = arrayDefinition->find_method("GetValue(System.Int32)");
		auto result = getValueMethod->invoke(array, { (void*)itemIndex });
		if (!result.exception_thrown)
		{
			if constexpr (std::is_same_v<T, reframework::API::ManagedObject*>)
			{
				return { static_cast<T>(result.ptr) };
			}
			else if constexpr (std::is_same_v<T, int>)
			{
				return { static_cast<T>(result.dword) };
			}
			else
			{
				LogError("Type not supported");
			}
		}
		else
		{
			LogError("Failed to get field value at index: " + itemIndex);
		}
		//return { getValueMethod->call<T>(api->get_vm_context(), array, itemIndex) };

	}
}
