#pragma once
#include "Constants.h"
#include <string>
#include <vector>
struct Character
{
public:
	Character(Fight::PlayerType type, int style) : Type(type), FightStyle(style) 
	{};

	Fight::PlayerType Type;
	int FightStyle;
	bool Equals(const Character& c) { return c.FightStyle == FightStyle && c.Type == Type; }
	std::string ToString() { return std::string("Type: ") + std::to_string(Type) + std::string(" - FightStyle: ") + std::to_string(FightStyle); }

	//todo make this more generic to support more characters;
	bool ShouldOverrideTrack() { return Type == Fight::PlayerType::Kimberly && FightStyle == 8;	}
	Game::Screen GetSimpleTrackOverrideScreenId() { return Game::Screen::KimberlyLvl3; }
	Game::Screen GetBothTrackOverrideScreenId() { return Game::Screen::KimberlyLvl3Both; }

	static std::vector<Game::Screen> GetTrackOverrideScreenIds() {
		return std::vector<Game::Screen>
		{Game::Screen::KimberlyLvl3, Game::Screen::KimberlyLvl3Both};
	};

	static bool IsTrackOverrideScreenId(Game::Screen screenId) {
		for (Game::Screen s : GetTrackOverrideScreenIds())
		{
			if (s == screenId)
			{
				return true;
			}
		}
		return false;
	};
};

