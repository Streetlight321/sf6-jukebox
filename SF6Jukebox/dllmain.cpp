// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <string>
#include "include/reframework/API.hpp"
#include "Logger.h"
#include "GameStateAPI.h"
#include "PlayerAPI.h"
#include "Settings.h"
#include "PerfTracker.h"

void on_present()
{
    PERF_TRACKING("on_present", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    GameStateAPI::GetInstance()->Update();    
}

extern "C" __declspec(dllexport) bool reframework_plugin_initialize(const REFrameworkPluginInitializeParam * param) 
{
    reframework::API::initialize(param);
    
    if (Settings::GetInstance()->IsEnabled())
    {   
        const auto functions = param->functions;
        param->functions->on_present(on_present);
        PlayerAPI::GetInstance()->resetCharacterPlaylist();
        PlayerAPI::GetInstance()->resetStagePlaylist();
        PlayerAPI::GetInstance()->resetFlowScenePlaylist();
        PlayerAPI::GetInstance()->resetCustomScreenPlaylist();
    }
    else
    {
        Logger::GetInstance()->LogInfo("Mod is disabled.");
    }

    return true;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

