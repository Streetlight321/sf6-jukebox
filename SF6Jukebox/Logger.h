#pragma once
#include <format>
#include <string>

class Logger
{
public:
	Logger(Logger& other) = delete;
	void operator=(const Logger&) = delete;

	void LogInfo(std::string message);
	void LogWarning(std::string message);
	void LogError(std::string message);

	static Logger* GetInstance();

	

protected:
	Logger();

	static Logger* instance;

private:

	void RegisterLogEvents();
};


template<typename... Args>
inline void LogInfo(std::string message, Args... args)
{
	std::string m = std::vformat(message, std::make_format_args(args...));
	Logger::GetInstance()->LogInfo(m);
}

template<typename... Args>
inline void LogWarning(std::string message, Args... args)
{
	std::string m = std::vformat(message, std::make_format_args(args...));
	Logger::GetInstance()->LogWarning(m);
}

template<typename... Args>
inline void LogError(std::string message, Args... args)
{
	std::string m = std::vformat(message, std::make_format_args(args...));
	Logger::GetInstance()->LogError(m);
}