#pragma once
#include "tinyxml2.h"
#include <filesystem>
#include <map>
#include "FlowScene.h"

class Settings
{
public:
	Settings(Settings& other) = delete;
	void operator=(const Settings&) = delete;

	static Settings* GetInstance();

	bool IsEnabled() { return mIsEnabled; }
	bool IsOverlayEnabled (){ return mIsOverlayEnabled; }
	bool IsOverrideMenusTracksEnabled() { return mIsOverrideMenusTracksEnabled; }

	struct KeyBindings
	{
		int NextTrack;
		int PreviousTrack;
		int VolumeUp;
		int VolumeDown;
		int Restart;
		int ShowOverlay;

	} Shortcuts;

	enum PlaylistPriority : int
	{
		Random = 0,
		Stage = 1,
		P1 = 2,
		P2 = 3,
		Player = 4,
		Opponent = 5
	} FightPlaylistPriority;

	std::map<Game::FlowScene, FlowScene> FlowScenes;

protected:
	Settings();

	static Settings* instance;

private:

	enum Version
	{
		Older,
		Unknown,
		v1_21_0,
		v1_22_0,
		v1_23_0,
		v1_24_0,
		v1_25_0,
		v1_25_1,
		v1_25_2
	};

	std::filesystem::path GetFilePath();

	Version GetVersion();
	void LoadDefault();
	void Load_1_21_0();
	void Load_1_22_0();
	void Load_1_23_0();
	void Load_1_24_0();
	void Load_1_25_0();
	void Load_1_25_1();
	void Load_1_25_2();
	bool mIsEnabled = false;
	bool mIsOverlayEnabled = false;
	bool mIsOverrideMenusTracksEnabled = false;
};

