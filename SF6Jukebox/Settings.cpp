#include "pch.h"
#include "Settings.h"
#include "Logger.h"
#include "ModuleLoader.h"
#include <string>


Settings* Settings::instance = nullptr;

Settings::Settings()
{
	switch (GetVersion())
	{
		case Settings::Version::Unknown:
			Logger::GetInstance()->LogInfo("Unknown settings version.");
			LoadDefault();
			break;

		case Settings::Version::Older:
			Logger::GetInstance()->LogInfo("Loading older settings.");
			LoadDefault();
			break;

		case Settings::Version::v1_21_0:
			Logger::GetInstance()->LogInfo("Loading settings v1.21.0");
			Load_1_21_0();
			break;

		case Settings::Version::v1_22_0:
			Logger::GetInstance()->LogInfo("Loading settings v1.22.0");
			Load_1_22_0();
			break;

		case Settings::Version::v1_23_0:
			Logger::GetInstance()->LogInfo("Loading settings v1.23.0");
			Load_1_23_0();
			break;

		case Settings::Version::v1_24_0:
			Logger::GetInstance()->LogInfo("Loading settings v1.24.0");
			Load_1_24_0();
			break;

		case Settings::Version::v1_25_0:
			Logger::GetInstance()->LogInfo("Loading settings v1.25.0");
			Load_1_25_0();
			break;

		case Settings::Version::v1_25_1:
			Logger::GetInstance()->LogInfo("Loading settings v1.25.1");
			Load_1_25_1();
			break;

		case Settings::Version::v1_25_2:
			Logger::GetInstance()->LogInfo("Loading settings v1.25.2");
			Load_1_25_2();
			break;
	}
}

std::filesystem::path Settings::GetFilePath()
{
	return std::filesystem::path{ ModuleLoader::GetInstance()->GetModulePath() } / std::filesystem::path{ "settings.xml" };	
}

Settings::Version Settings::GetVersion()
{
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		auto doc = new tinyxml2::XMLDocument();
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* versionElement = settingsElement->FirstChildElement("Version");
		if (versionElement)
		{
			std::string VersionString = std::string(versionElement->GetText());
			if (VersionString == "1.21.0")
			{
				return Settings::Version::v1_21_0;
			}
			else if (VersionString == "1.22.0")
			{
				return Settings::Version::v1_22_0;
			}
			else if (VersionString == "1.23.0")
			{
				return Settings::Version::v1_23_0;
			}
			else if (VersionString == "1.24.0")
			{
				return Settings::Version::v1_24_0;
			}
			else if (VersionString == "1.25.0")
			{
				return Settings::Version::v1_25_0;
			}
			else if (VersionString == "1.25.1")
			{
				return Settings::Version::v1_25_1;
			}
			else if (VersionString == "1.25.2")
			{
				return Settings::Version::v1_25_2;
			}
			else
			{
				return Settings::Version::Unknown;
			}
		}
		else
		{
			return Settings::Version::Older;
		}
	}
	else
	{
		return Settings::Version::Unknown;
	}
}

void Settings::LoadDefault()
{
	mIsEnabled = true;
	Shortcuts.NextTrack = 33;
	Shortcuts.PreviousTrack = 34;
	Shortcuts.VolumeUp = 187;
	Shortcuts.VolumeDown = 189;
}

void Settings::Load_1_21_0()
{
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* enabledElement = settingsElement->FirstChildElement("Enabled");
		tinyxml2::XMLElement* shortcutsElement = settingsElement->FirstChildElement("Shortcuts");
		tinyxml2::XMLElement* nextTrackElement = shortcutsElement->FirstChildElement("NextTrack");
		tinyxml2::XMLElement* previousTrackElement = shortcutsElement->FirstChildElement("PreviousTrack");
		tinyxml2::XMLElement* volumeUpElement = shortcutsElement->FirstChildElement("VolumeUp");
		tinyxml2::XMLElement* volumeDownElement = shortcutsElement->FirstChildElement("VolumeDown");
		Shortcuts.NextTrack = atoi(nextTrackElement->GetText());
		Shortcuts.PreviousTrack = atoi(previousTrackElement->GetText());
		Shortcuts.VolumeUp = atoi(volumeUpElement->GetText());
		Shortcuts.VolumeDown = atoi(volumeDownElement->GetText());
		mIsEnabled = atoi(enabledElement->GetText()) == 1;
	}
}

void Settings::Load_1_22_0()
{
	Load_1_21_0();
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* flowScenesElement = settingsElement->FirstChildElement("FlowScenes");
		tinyxml2::XMLNode* flowSceneElement = flowScenesElement->FirstChildElement("FlowScene");
		do
		{
			auto flowSceneIdElement = flowSceneElement->FirstChildElement("Id");	
			auto flowSceneEnabledElement = flowSceneElement->FirstChildElement("Enabled");
			int flowSceneId = atoi(flowSceneIdElement->GetText());
			int enabled = atoi(flowSceneEnabledElement->GetText());
			FlowScenes[(Game::FlowScene)flowSceneId] = FlowScene((Game::FlowScene)flowSceneId, enabled == 1);
		} while ((flowSceneElement = flowSceneElement->NextSibling()) != nullptr);
		
	}
}

void Settings::Load_1_23_0()
{
	Load_1_22_0();
}

void Settings::Load_1_24_0()
{
	Load_1_23_0();
}

void Settings::Load_1_25_0()
{
	Load_1_24_0();
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* overlayEnabledElement = settingsElement->FirstChildElement("OverlayEnabled");
		tinyxml2::XMLElement* playlistPriorityElement = settingsElement->FirstChildElement("PlaylistPriority");
		tinyxml2::XMLElement* overrideMenusTracksElement = settingsElement->FirstChildElement("OverrideMenusTracks");
		tinyxml2::XMLElement* shortcutsElement = settingsElement->FirstChildElement("Shortcuts");
		tinyxml2::XMLElement* showOverlayElement = shortcutsElement->FirstChildElement("ShowOverlay");		
		tinyxml2::XMLElement* restartElement = shortcutsElement->FirstChildElement("Restart");
		Shortcuts.ShowOverlay = atoi(showOverlayElement->GetText());
		Shortcuts.Restart = atoi(restartElement->GetText());
		mIsOverlayEnabled = atoi(overlayEnabledElement->GetText());
		FightPlaylistPriority = static_cast<PlaylistPriority>(atoi(playlistPriorityElement->GetText()));
		mIsOverrideMenusTracksEnabled = atoi(overrideMenusTracksElement->GetText());
	}
}

void Settings::Load_1_25_1()
{
	Load_1_25_0();
}

void Settings::Load_1_25_2()
{
	Load_1_25_0();
}

Settings* Settings::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Settings();
	}

	return instance;
}
