#pragma once
#include "Constants.h"

struct BattleDescription
{
public:
	BattleDescription(Fight::PlayerType player, Fight::PlayerType opponent)
		: PlayerCharacter(player), OpponentCharacter(opponent)
	{}

	Fight::PlayerType GetPlayerCharacter() const
	{ 
		return PlayerCharacter; 
	}

	Fight::PlayerType GetOpponentCharacter() const
	{
		return OpponentCharacter;
	}

private:
	Fight::PlayerType PlayerCharacter;
	Fight::PlayerType OpponentCharacter;
};

