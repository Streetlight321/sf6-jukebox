#pragma once
#include "Constants.h"
#include "Character.h"
#include "BattleDescription.h"
#include "Settings.h"
#include <vector>
#include <set>>
#include <optional>

class PlayerAPI;
class Settings;

class GameStateAPI
{
public:
	GameStateAPI(GameStateAPI& other) = delete;
	void operator=(const GameStateAPI&) = delete;

	static GameStateAPI* GetInstance();

	void Update();



protected:
	GameStateAPI();
	PlayerAPI* playerAPI;

	static GameStateAPI* instance;

private:
	void PlayForFlowScene(Game::FlowScene flowScene);
	void HandleFlowScene();
	bool IsWorldTourFight();
	void HandleFightState();
	bool HandleCharacterState();
	void HandleShortcuts();
	bool WasPreviousFlowSceneFight();
	bool IsFlowSceneEnabled(Game::FlowScene flowScene);
	void MuteBGM(Game::FlowScene flowScene);
	void MuteSpectateBGM();
	void MuteInteractiveFightBGM();
	void HandleSpectateMode(Game::FlowScene flowScene);

	Character GetCharacter(int playerIndex);
	BattleDescription GetBattleDescription();
	Fight::State GetFightState();
	Fight::Stage GetStage();
	Game::Mode GetGameMode();
	Game::FlowScene GetFlowScene();

	Game::FlowScene LastFlowScene = Game::FlowScene::eNone;
	Fight::State LastFightState = Fight::State::StageInit;

	// All the possible flowscene we can transition to out of a fight.
	std::set<Game::FlowScene> OutOfFightFlowScenes
	{
		Game::FlowScene::eModeSelect,
		Game::FlowScene::eBattleHubMain,
		Game::FlowScene::eBattleFighterSelect,
		Game::FlowScene::eArcadeFighterSelect,
		Game::FlowScene::eWorldTourCity,
		Game::FlowScene::eCustomRoomMain,
		Game::FlowScene::eBattleStageSelect,
		Game::FlowScene::eESportsMainMenu,
		Game::FlowScene::eModeSelect,
		Game::FlowScene::eArcadeStageMove,
		Game::FlowScene::eTutorialSetup
	};

	std::optional<Settings::PlaylistPriority> CurrentRandomPlaylistPriority = std::nullopt;

	Character* lastP1CharacterState; 
	Character* lastP2CharacterState;

	Settings* settings = nullptr;

	bool WasVolumeUpPressed = false;
	bool WasVolumeDownPressed = false; 
	bool WasNextTrackPressed = false;
	bool WasPreviousTrackPressed = false;
	bool WasShowOverlayPressed = false;
	bool WasRestartPressed = false;
	bool IsCharacterStateOverrideDetected = false;
	bool IsSpectating = false;
};

