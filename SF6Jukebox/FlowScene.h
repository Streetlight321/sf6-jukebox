#pragma once
#include <string>
#include "Constants.h"

class FlowScene
{
public:
	FlowScene() = default;
	FlowScene(Game::FlowScene id, bool enabled) : Id(id), Enabled(enabled)
	{};

	bool IsEnabled() { return Enabled; }
	Game::FlowScene GetId() { return Id; }

private:
	bool Enabled;
	Game::FlowScene Id;
};

