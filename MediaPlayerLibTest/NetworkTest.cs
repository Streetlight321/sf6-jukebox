using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Message;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;

namespace MediaPlayerLibTest
{
    public class NetworkTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TrackUpdateMessageTest()
        {
            AutoResetEvent locker = new AutoResetEvent(false);
            var logger = new DebugLogger();
            OverlayServer server = new OverlayServer(1212, logger);
            server.Start();
            OverlayClient client = new OverlayClient(1212, logger);
            int messageCount = 100;
            List<string> messages = new List<string>();
            for(int i = 0; i < messageCount; i++)
            {
                messages.Add($"Track{i}");
            }

            int messageReceived = 0;

            client.OnTrackUpdateMessage += (a, b) =>
            {
                Assert.IsTrue(b.Track == $"Track{messageReceived}");
                if(messageReceived == messageCount-1)
                {
                    locker.Set();
                }
                messageReceived++;
            };
            client.Start();
            
            foreach(var message in messages)
            {
                server.Send(new TrackUpdateMessage { Track = message });
            }
            
            locker.WaitOne(TimeSpan.FromSeconds(5));            
        }
    }
}