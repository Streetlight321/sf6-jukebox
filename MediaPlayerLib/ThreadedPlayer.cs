﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;
using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Message;
using NAudio.Wave;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace MediaPlayerLib
{
    public class ThreadedPlayer
    {
        AudioFileReader audioFile;
        WaveOutEvent outputDevice;
        private float volume;
        

        public ThreadedPlayer(Settings settings, Logger logger)
        {
            Logger = logger;
            Settings = settings;
            PlaybackRequests = new ConcurrentQueue<PlaybackRequest> ();
            RequestsProcessorThread = new Thread(Start);
            Volume = settings.Volume;
            

            if (Settings.OverlayEnabled == 1)
            {
                OverlayServer = new OverlayServer(1212, Logger);
                OverlayServer.Start();
            }

            RequestsProcessorThread.Start();
        }

        private int OverlayUpdateFrequency => 1000;
        private DateTime LastOverlayUpdate { get; set; } = DateTime.Now;
        private ConcurrentQueue<PlaybackRequest> PlaybackRequests { get; }
        private Thread RequestsProcessorThread { get; }
        public Settings Settings { get; }
        private Logger Logger { get; }
        public bool IsPlaying { get; internal set; }
        bool IsOverlayStarted = false;
        private OverlayServer OverlayServer { get; set; }
        PlaylistType CurrentPlaylistType = PlaylistType.None;
        StageId CurrentStageId { get; set; }
        FlowSceneId CurrentFlowSceneId { get; set; }
        CustomScreenId CurrentCustomScreenId { get; set; }
        CharacterId CurrentCharacterId { get; set; }
        Dictionary<StageId, Playlist> StagePlaylists { get; set; }
        Dictionary<FlowSceneId, Playlist> FlowScenePlaylists { get; set; }
        Dictionary<CustomScreenId, Playlist> CustomScreenPlaylists { get; set; }
        Dictionary<CharacterId, Playlist> CharacterPlaylists { get; set; }


        public float Volume
        {
            get => volume;
            set
            {
                volume = value;
                Settings.Volume = value;
            }
        }

        private void Start()
        {        
            while (true)
            {
                if(audioFile != null)
                {
                    audioFile.Volume = Volume;
                    if (IsOverlayStarted)
                    {
                        SendTrackUpdateMessage(new TrackUpdateMessage() { Track = audioFile.FileName });
                    }
                }

                if (PlaybackRequests.Count > 0)
                {
                    if(PlaybackRequests.TryDequeue(out PlaybackRequest playbackRequest))
                    {
                        var stopRequest = playbackRequest as StopRequest;
                        var playRequest = playbackRequest as PlayRequest;
                        var positionRequest = playbackRequest as SetPositionRequest;
                        
                        if (stopRequest != null)
                        {
                            StopInternal();
                        }
                        
                        if(playRequest != null)
                        {
                            PlayInternal(playRequest);
                        }

                        if(positionRequest != null)
                        {
                            SetPositionInternal(positionRequest.Position);
                        }

                        // Giving NAudio some breathing room...
                        Thread.Sleep(100);
                    }

                    if (IsPlaying && audioFile != null && !string.IsNullOrEmpty(audioFile.FileName))
                    {
                        SendTrackUpdateMessage(new TrackUpdateMessage() { Track = audioFile.FileName });
                    }
                }
            }
        }

        public void QueueInStageNextTrack()
        {
            Logger.Log("QueueInStageNextTrack");
            StagePlaylists[CurrentStageId].QueueInNextTrack();
        }

        public void QueuePlaybackRequest(PlaybackRequest request)
        {
            PlaybackRequests.Enqueue(request);
        }

        private void Init(string trackFile)
        {
            Logger.Log("Init");

            if (!File.Exists(trackFile))
            {
                Logger.Log(String.Format("Error, could not find track {0}", trackFile));
                return;
            }

            audioFile = new AudioFileReader(trackFile);
            outputDevice = new WaveOutEvent();
            outputDevice.PlaybackStopped += OutputDevice_PlaybackStopped;
            audioFile.Position = 0;
            audioFile.Volume = Volume;
            if (!IsOverlayStarted)
            {
                StartOverlay();
            }
        }

        public void SetPosition(long position)
        {
            QueuePlaybackRequest(new SetPositionRequest(position));
        }

        private void SetPositionInternal(long position)
        {
            if (IsPlaying && audioFile != null)
            {
                audioFile.Position = position;
            }
        }

        private Playlist GetCurrentPlaylist()
        {
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    return StagePlaylists[CurrentStageId];
                case PlaylistType.FlowScene:
                    return FlowScenePlaylists[CurrentFlowSceneId];
                case PlaylistType.CustomScreen:
                    return CustomScreenPlaylists[CurrentCustomScreenId];
                case PlaylistType.Character:
                    return CharacterPlaylists[CurrentCharacterId];
                default:
                    return null;
            }
        }

        private void OutputDevice_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            Logger.Log("OutputDevice_PlaybackStopped");
            var currentPlayMode = GetCurrentPlaylist().InnerPlaylist.Mode;
            switch (currentPlayMode)
            {
                case PlayMode.Repeat:
                    PlayNextTrack();
                    break;
                case PlayMode.Once:
                    QueuePlaybackRequest(new StopRequest());
                    if (!GetCurrentPlaylist().IsLastTrack())
                    {
                        PlayNextTrack();
                    }
                    break;
                case PlayMode.SingleRepeat:
                    switch (CurrentPlaylistType)
                    {
                        case PlaylistType.Stage:
                            QueuePlaybackRequest(new PlayRequest(new Track(audioFile.FileName, CurrentStageId)));
                            break;
                        case PlaylistType.FlowScene:
                            QueuePlaybackRequest(new PlayRequest(new Track(audioFile.FileName, CurrentFlowSceneId)));
                            break;
                        case PlaylistType.CustomScreen:
                            QueuePlaybackRequest(new PlayRequest(new Track(audioFile.FileName, CurrentCustomScreenId)));
                            break;
                        case PlaylistType.Character:
                            QueuePlaybackRequest(new PlayRequest(new Track(audioFile.FileName, CurrentCharacterId)));
                            break;
                    }
                    break;
                case PlayMode.SingleOnce:
                    QueuePlaybackRequest(new StopRequest());
                    break;
                default:
                    break;
            }
        }

        public void PlayNextTrack()
        {
            Logger.Log($"PlayNextTrack for {CurrentPlaylistType}");

            QueuePlaybackRequest(new StopRequest());
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentStageId)));
                    break;
                case PlaylistType.FlowScene:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentFlowSceneId)));
                    break;
                case PlaylistType.CustomScreen:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentCustomScreenId)));
                    break;
                case PlaylistType.Character:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentCharacterId)));
                    break;
            }
        }

        public void PlayPreviousTrack()
        {
            Logger.Log($"PlayPreviousTrack for {CurrentPlaylistType}");

            QueuePlaybackRequest(new StopRequest());
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentStageId)));
                    break;
                case PlaylistType.FlowScene:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentFlowSceneId)));
                    break;
                case PlaylistType.CustomScreen:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentCustomScreenId)));
                    break;
                case PlaylistType.Character:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentCharacterId)));
                    break;
            }
        }

        public void Play(Track track)
        {
            QueuePlaybackRequest(new PlayRequest(track));
        }

        public void Stop()
        {
            QueuePlaybackRequest(new StopRequest());
        }

        public void PlayCharacterTrack(int characterId)
        {
            Logger.Log(String.Format("Play for character {0}", characterId));
            if (CharacterPlaylists.ContainsKey((CharacterId)characterId))
            {
                if (CharacterPlaylists[(CharacterId)characterId].Tracks.Any())
                {
                    Play(new Track(CharacterPlaylists[(CharacterId)characterId].GetNextTrack(), (CharacterId)characterId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(CharacterId)characterId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for character id {(CharacterId)characterId} not found.");
            }
        }

        public void PlayStageTrack(int stageId)
        {
            Logger.Log(String.Format("Play for stage {0}", stageId));
            if (StagePlaylists.ContainsKey((StageId)stageId))
            {
                if (StagePlaylists[(StageId)stageId].Tracks.Any())
                {
                    Play(new Track(StagePlaylists[(StageId)stageId].GetNextTrack(), (StageId)stageId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(StageId)stageId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for stage id {(StageId)stageId} not found.");
            }
        }

        public void PlayFlowSceneTrack(int flowSceneId)
        {
            Logger.Log(String.Format("Play for flowscene {0}", flowSceneId));
            if (FlowScenePlaylists.ContainsKey((FlowSceneId)flowSceneId))
            {
                if (FlowScenePlaylists[(FlowSceneId)flowSceneId].Tracks.Any())
                {
                    Play(new Track(FlowScenePlaylists[(FlowSceneId)flowSceneId].GetNextTrack(), (FlowSceneId)flowSceneId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(FlowSceneId)flowSceneId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for flowscene id {(FlowSceneId)flowSceneId} not found.");
            }
        }

        public void PlayCustomScreenTrack(int customScreenId)
        {
            Logger.Log(String.Format("Play for custom screen {0}", customScreenId));
            if (CustomScreenPlaylists.ContainsKey((CustomScreenId)customScreenId))
            {
                if (CustomScreenPlaylists[(CustomScreenId)customScreenId].Tracks.Any())
                {
                    Play(new Track(CustomScreenPlaylists[(CustomScreenId)customScreenId].GetNextTrack(), (CustomScreenId)customScreenId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(CustomScreenId)customScreenId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for custom screen id {(CustomScreenId)customScreenId} not found.");
            }
        }

        private void PlayInternal(PlayRequest playRequest)
        {
            if (string.IsNullOrEmpty(playRequest.Track.Path))
            {
                Logger.Log("Aborting playback, provided path was empty.");
                return;
            }
            else if (!File.Exists(playRequest.Track.Path))
            {
                Logger.Log($"Aborting playback, file does not exist: {playRequest.Track.Path}");
                return;
            }

            if (IsPlaying)
            {
                StopInternal();
            }

            if (playRequest.Track.Path != String.Empty || !File.Exists(playRequest.Track.Path))
            {
                Logger.Log(String.Format("PlayInternal {0}", playRequest.Name));
                CurrentPlaylistType = playRequest.Track.PlaylistType;
                switch (playRequest.Track.PlaylistType)
                {
                    case PlaylistType.Stage:
                        CurrentStageId = playRequest.Track.StageId;
                        break;

                    case PlaylistType.FlowScene:
                        CurrentFlowSceneId = playRequest.Track.FlowSceneId;
                        break;

                    case PlaylistType.CustomScreen:
                        CurrentCustomScreenId = playRequest.Track.CustomScreenId;
                        break;

                    case PlaylistType.Character:
                        CurrentCharacterId = playRequest.Track.CharacterId;
                        break;
                }

                Init(playRequest.Track.Path);
                outputDevice.Init(audioFile);
                outputDevice.Play();
                SendTrackUpdateMessage(new TrackUpdateMessage { Track = playRequest.Track.Path, Progress = 0 });
                if (playRequest.Track.PlaylistType == PlaylistType.Stage || playRequest.Track.PlaylistType == PlaylistType.Character)
                {
                    ShowOverlay();
                }
                IsPlaying = true;
            }
            else
            {
                Logger.Log("PlayInternal - No track to play.");
            }

            IsPlaying = true;
        }

        private void StopInternal()
        {
            Logger.Log("StopInternal");
            FadeOut();

            if (IsPlaying)
            {
                if (outputDevice != null)
                {
                    outputDevice.PlaybackStopped -= OutputDevice_PlaybackStopped;
                }

                string fileName = audioFile.FileName;
                outputDevice?.Stop();
                outputDevice?.Dispose();
                audioFile?.Dispose();
                outputDevice = null;
                audioFile = null;
                IsPlaying = false;

                Logger.Log($"Stopped {fileName}");
            }
            else
            {
                Logger.Log("Nothing to stop.");
            }
        }

        private float GetFadeOutDuration()
        {
            float result = 0;
            if (CurrentPlaylistType != PlaylistType.None)
            {
                var currentPlaylist = GetCurrentPlaylist();

                if (currentPlaylist != null)
                {
                    result = currentPlaylist.InnerPlaylist.FadeOutDuration;
                }
            }

            return result;
        }

        private void FadeOut()
        {
            Logger.Log("FadeOut");
            if (IsPlaying)
            {
                Logger.Log($"Fading out {audioFile.FileName}");
                float fadeoutSleepStep = 50;
                float fadeoutDuration = GetFadeOutDuration() * 1000;
                if (fadeoutDuration <= fadeoutSleepStep)
                {
                    return;
                }

                int iterations = Convert.ToInt32(fadeoutDuration / fadeoutSleepStep);
                float volumeDownStep = audioFile.Volume / iterations;

                for (int i = 0; i < iterations; i++)
                {
                    audioFile.Volume -= volumeDownStep;
                    Thread.Sleep((int)fadeoutSleepStep);
                }
            }
            else
            {
                Logger.Log("Nothing to fade out.");
            }
        }

        private void StartOverlay()
        {
            if (Settings.OverlayEnabled == 1)
            {
                Logger.Log("Starting Overlay");
                var refDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
                var overlayPath = Path.Combine(refDir.Parent.FullName, @"SF6JukeboxOverlay\SF6JukeboxOverlay.exe");
                Process.Start(new ProcessStartInfo(overlayPath));
                IsOverlayStarted = true;
            }
        }

        public void ShowOverlay()
        {
            if (Settings.OverlayEnabled == 1)
            {
                OverlayServer.Send(new ShowOverlayMessage());
            }
        }

        private void SendTrackUpdateMessage(TrackUpdateMessage trackUpdateMessage)
        {
            TimeSpan timeElapsedSinceLastUpdate = DateTime.Now - LastOverlayUpdate;
            if(timeElapsedSinceLastUpdate < TimeSpan.FromMilliseconds(OverlayUpdateFrequency))
            {
                return;
            }

            if (Settings.OverlayEnabled == 1)
            {
                if (audioFile != null)
                {
                    float pos = audioFile.Position;
                    float length = audioFile.Length;
                    trackUpdateMessage.Progress = pos / length;
                }
                OverlayServer.Send(new TrackUpdateMessage { Track = trackUpdateMessage.Track, Progress = trackUpdateMessage.Progress });
                LastOverlayUpdate = DateTime.Now;
            }
        }

        public void ResetFlowScenePlaylist()
        {
            Logger.Log("ResetFlowScenePlaylist");

            try
            {
                FlowScenePlaylists = new Dictionary<FlowSceneId, Playlist>();
                foreach (var flowScene in Settings.FlowScenes)
                {
                    if (flowScene.IsRandomPlaylist == 1)
                    {
                        FlowScenePlaylists[flowScene.FlowSceneId] = new Playlist(flowScene.Playlist) { Id = (int)flowScene.Id, Tracks = Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList() };
                    }
                    else
                    {
                        if (!FlowScenePlaylists.ContainsKey(flowScene.FlowSceneId))
                        {
                            FlowScenePlaylists[flowScene.FlowSceneId] = new Playlist(flowScene.Playlist) { Id = (int)flowScene.Id };
                        }

                        FlowScenePlaylists[flowScene.FlowSceneId].Tracks = flowScene.Playlist.Tracks.Select(t => t.Path).ToList();
                    }

                    FlowScenePlaylists[flowScene.FlowSceneId].Shuffle();
                    LogPlaylist(FlowScenePlaylists[flowScene.FlowSceneId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetStagePlaylist()
        {
            Logger.Log("ResetStagePlaylist");

            try
            {
                StagePlaylists = new Dictionary<StageId, Playlist>();
                foreach (var customScreen in Settings.Stages)
                {
                    if (customScreen.IsRandomPlaylist == 1)
                    {
                        StagePlaylists[customScreen.StageId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id, Tracks = Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList() };
                    }
                    else
                    {
                        if (!StagePlaylists.ContainsKey(customScreen.StageId))
                        {
                            StagePlaylists[customScreen.StageId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id };
                        }

                        StagePlaylists[customScreen.StageId].Tracks = customScreen.Playlist.Tracks.Select(t => t.Path).ToList();
                    }

                    StagePlaylists[customScreen.StageId].Shuffle();
                    LogPlaylist(StagePlaylists[customScreen.StageId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetCharacterPlaylist()
        {
            Logger.Log("ResetCharacterPlaylist");

            try
            {
                CharacterPlaylists = new Dictionary<CharacterId, Playlist>();
                foreach (var character in Settings.Characters)
                {
                    if (character.IsRandomPlaylist == 1)
                    {
                        CharacterPlaylists[character.CharacterId] = new Playlist(character.Playlist) { Id = character.Id, Tracks = Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList() };
                    }
                    else
                    {
                        if (!CharacterPlaylists.ContainsKey(character.CharacterId))
                        {
                            CharacterPlaylists[character.CharacterId] = new Playlist(character.Playlist) { Id = character.Id };
                        }

                        CharacterPlaylists[character.CharacterId].Tracks = character.Playlist.Tracks.Select(t => t.Path).ToList();
                    }

                    CharacterPlaylists[character.CharacterId].Shuffle();
                    LogPlaylist(CharacterPlaylists[character.CharacterId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetCustomScreensPlaylist()
        {
            Logger.Log("ResetCustomScreensPlaylist");

            try
            {
                CustomScreenPlaylists = new Dictionary<CustomScreenId, Playlist>();
                foreach (var customScreen in Settings.CustomScreens)
                {
                    if (customScreen.IsRandomPlaylist == 1)
                    {
                        CustomScreenPlaylists[customScreen.CustomScreenId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id, Tracks = Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList() };
                    }
                    else
                    {
                        if (!CustomScreenPlaylists.ContainsKey(customScreen.CustomScreenId))
                        {
                            CustomScreenPlaylists[customScreen.CustomScreenId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id };
                        }

                        CustomScreenPlaylists[customScreen.CustomScreenId].Tracks = customScreen.Playlist.Tracks.Select(t => t.Path).ToList();
                    }

                    CustomScreenPlaylists[customScreen.CustomScreenId].Shuffle();
                    LogPlaylist(CustomScreenPlaylists[customScreen.CustomScreenId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }


        public void LogPlaylist(Playlist playlist)
        {
            Logger.Log($"-- Dumping playlist with id {playlist.Id} -- ");
            for (int i = 0; i < playlist.Tracks.Count; i++)
            {
                Logger.Log(String.Format("{0}{1}", i == playlist.CurrentTrackIndex ? "-> " : String.Empty, playlist.Tracks[i]));
            }
            Logger.Log($"------------------------------------------------------------");
        }
    }
}
