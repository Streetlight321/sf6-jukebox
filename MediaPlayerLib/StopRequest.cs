﻿namespace MediaPlayerLib
{
    public class StopRequest : PlaybackRequest
    {
        public StopRequest()
        {            
        }

        public override string Name => "Stop";
    }
}
