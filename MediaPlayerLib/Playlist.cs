﻿using MediaPlayerDAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public class Playlist
    {
        public Playlist(MediaPlayerDAL.Model.Playlist innerPlaylist) 
        {
            Tracks = new List<string>();
            CurrentTrackIndex = -1;
            InnerPlaylist = innerPlaylist;
        }

        public int Id { get; set; }
        public int CurrentTrackIndex { get; set; }
        public List<string> Tracks { get; set; }
        public MediaPlayerDAL.Model.Playlist InnerPlaylist { get; }

        public void Shuffle()
        {
            List<string> tracks = new List<string>(Tracks);
            List<string> shuffledTracks = new List<string>();
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < tracks.Count; i++)
            {
                int trackIndex = r.Next(tracks.Count);
                shuffledTracks.Add(tracks[trackIndex]);
                tracks.RemoveAt(trackIndex);
                i--;
            }

            Tracks = shuffledTracks;
        }

        public string GetCurrentTrack()
        {
            if(Tracks.Any() && CurrentTrackIndex != -1)
            {
                return Tracks[CurrentTrackIndex];
            }
            
            return null;
        }

        public bool IsLastTrack()
        {
            return CurrentTrackIndex == Tracks.Count - 1;
        }

        public void QueueInNextTrack()
        {
            if (Tracks.Count == 0)
            {
                return;
            }

            if (CurrentTrackIndex == Tracks.Count - 1)
            {
                CurrentTrackIndex = 0;
            }
            else
            {
                CurrentTrackIndex++;
            }
        }

        public string GetNextTrack()
        {
            if(Tracks.Count == 0)
            {
                return string.Empty;
            }

            if(CurrentTrackIndex == Tracks.Count-1) 
            {
                CurrentTrackIndex = 0;
            }
            else
            {
                CurrentTrackIndex++;
            }

            return Tracks[CurrentTrackIndex];
        }

        public string GetPreviousTrack()
        {
            if (Tracks.Count == 0)
            {
                return string.Empty;
            }

            if (CurrentTrackIndex == 0)
            {
                CurrentTrackIndex = Tracks.Count - 1;
            }
            else
            {
                CurrentTrackIndex--;
            }

            return Tracks[CurrentTrackIndex];
        }        
    }
}
