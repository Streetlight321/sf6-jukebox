﻿using MediaPlayerLib.Network.Contract;
using System;

namespace MediaPlayerLib
{
    public class Logger : ILogger
    {
        public event EventHandler OnLogRequested;

        public void Log(string message)
        {
            message = $"[Managed] - {message}";
            OnLogRequested?.Invoke(this, new LogEventArgs(message));
        }
    }
}
