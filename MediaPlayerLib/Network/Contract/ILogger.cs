﻿namespace MediaPlayerLib.Network.Contract
{
    public interface ILogger
    {
        void Log(string message);
    }
}
