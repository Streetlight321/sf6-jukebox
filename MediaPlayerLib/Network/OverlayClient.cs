﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using MediaPlayerLib.Network.Message;
using System.Diagnostics;
using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network
{
    public class OverlayClient
    {
        private Thread ClientThread { get; }
        public int Port { get; }
        public ILogger Logger { get; }

        public event EventHandler<TrackUpdateMessage> OnTrackUpdateMessage;
        public event EventHandler<ShowOverlayMessage> OnShowOverlayMessage;
        public event EventHandler<string> OnNetworkError;
        public bool KeepListening { get; set; }

        public OverlayClient(int port, ILogger logger)
        {
            ClientThread = new Thread(StartInternal);
            Port = port;
            Logger = logger;
            KeepListening = true;
        }

        public void Start()
        {
            Logger.Log("Start");
            ClientThread.Start();
        }

        public void Stop()
        {
            Logger.Log("Stop");
            KeepListening = false;
        }

        private void StartInternal()
        {
            var ipEndPoint = new IPEndPoint(IPAddress.Loopback, Port);
            while (KeepListening)
            {
                try
                {
                    using (var client = new TcpClient())
                    {
                        Logger.Log("Connecting...");
                        client.Connect(ipEndPoint);
                        Logger.Log("Connected!");
                        bool keepListening = true;
                        Logger.Log("Waiting for stream...");
                        using (var stream = client.GetStream())
                        {
                            Logger.Log("Reading stream.");
                            while (keepListening)
                            {
                                int headerSize = ReadMessageSize(stream);
                                var header = ReadMessage<HeaderMessage>(stream, headerSize);
                                if(header != null && header.Type == MessageType.TrackUpdate)
                                {
                                    int trackUpdateSize = ReadMessageSize(stream);
                                    var trackUpdate = ReadMessage<TrackUpdateMessage>(stream, trackUpdateSize);
                                    OnTrackUpdateMessage?.Invoke(this, trackUpdate);
                                }
                                else if (header != null && header.Type == MessageType.ShowOverlay)
                                {
                                    int messageSize = ReadMessageSize(stream);
                                    var message = ReadMessage<ShowOverlayMessage>(stream, messageSize);
                                    OnShowOverlayMessage?.Invoke(this, message);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    OnNetworkError?.Invoke(this, ex.ToString());
                }
            }
        }

        int ReadMessageSize(NetworkStream stream)
        {
            Logger.Log("ReadMessageSize");
            var sizeBuffer = new byte[sizeof(int)];
            stream.Read(sizeBuffer, 0, sizeof(int));
            int messageSize = BitConverter.ToInt32(sizeBuffer, 0);
            Logger.Log($"MessageSize: {messageSize}");
            return messageSize;
        }

        T ReadMessage<T>(NetworkStream stream, int headerSize) where T : class
        {
            Logger.Log($"ReadMessage {typeof(T)}");
            T headerMessage = null;
            var buffer = new byte[headerSize];
            int read = stream.Read(buffer, 0, headerSize);
            MemoryStream memoryStream = new MemoryStream();
            memoryStream.Write(buffer, 0, headerSize);
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            var rawData = memoryStream.ToArray();
            var rawDataStr = Encoding.UTF8.GetString(rawData, 0, rawData.Length);
            Logger.Log($"Read message.\r\nSize: {read}\r\nContent:\r\n{rawDataStr}");
            using (var ms = new MemoryStream(rawData))
            {
                headerMessage = xmlSerializer.Deserialize(ms) as T;
            }

            return headerMessage;
        }
    }
}
