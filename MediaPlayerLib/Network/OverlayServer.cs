﻿using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System;
using MediaPlayerLib.Network.Contract;
using System.Collections.Generic;
using System.Linq;
using MediaPlayerLib.Network.Message;
using System.Runtime.InteropServices.ComTypes;

namespace MediaPlayerLib.Network
{
    public class OverlayServer
    {
        public OverlayServer(int port, ILogger logger)
        {
            Port = port;
            Logger = logger;
            Messages = new Queue<IMessage>();
            MessageQueueLocker = new Semaphore(1,1);
            ServerThread = new Thread(StartInternal);
            KeepListening = true;
        }

        public int Port { get; }
        public ILogger Logger { get; }
        public Semaphore MessageQueueLocker { get; }
        private Thread ServerThread { get; }
        public bool KeepListening { get; set; }

        public void Log(string message)
        {
            Logger.Log(message);
        }

        Queue<IMessage> Messages { get; }

        public void Send(IMessage message)
        {
            MessageQueueLocker.WaitOne();
            Messages.Enqueue(new HeaderMessage() { Type = message.GetMessageType()});
            Messages.Enqueue(message);
            MessageQueueLocker.Release();
        }

        public void Start()
        {
            ServerThread.Start();
        }

        public void Stop()
        {
            KeepListening = false;
        }

        public void StartInternal()
        {
            Log("Starting Overlay Server");
            var ipEndPoint = new IPEndPoint(IPAddress.Loopback, Port); ;
            TcpListener listener = new TcpListener(ipEndPoint);
            
            try
            {
                listener.Start();
                Log("Awaiting connection...");
                using (var handler = listener.AcceptTcpClient())
                {
                    Log("Connected");
                    using (var stream = handler.GetStream())
                    {
                        while (KeepListening)
                        {
                            if (Messages.Any())
                            {
                                MessageQueueLocker.WaitOne();
                                var headerMessage = Messages.Dequeue();
                                var innerMessage = Messages.Dequeue();
                                MessageQueueLocker.Release();
                                Send(stream, headerMessage);
                                Send(stream, innerMessage);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
            }
            finally
            {
                listener.Stop();
            }
        }

        private void Send(NetworkStream stream, IMessage message)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(message.GetType());
            using (var ms = new MemoryStream())
            {
                xmlSerializer.Serialize(ms, message);
                ms.Flush();
                var lengthInBytes = BitConverter.GetBytes(ms.Length);
                stream.Write(lengthInBytes, 0, sizeof(int));
                stream.Write(ms.ToArray(), 0, (int)ms.Length);
            }
        }
    }
}
