﻿
namespace MediaPlayerLib.Network.Message
{
    public class HeaderMessage : MediaPlayerLib.Network.Contract.IMessage
    {
        public MessageType Type { get; set; }

        public MessageType GetMessageType()
        {
            return MessageType.Header;
        }
    }
}
