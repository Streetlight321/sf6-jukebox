﻿namespace MediaPlayerLib.Network.Message
{
    public enum MessageType
    {
        Header,
        TrackUpdate,
        ShowOverlay
    }
}
