﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public class SetPositionRequest : PlaybackRequest
    {
        public SetPositionRequest(long position)
        {
            Position = position;
        }

        public long Position { get; }

        public override string Name => $"Position to {Position}";
    }
}
