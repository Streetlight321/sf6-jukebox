﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public class SimplePlayer
    {
        AudioFileReader audioFile;
        WaveOutEvent outputDevice;
        bool IsPlaying { get; set; }

        private void Init(string trackFile)
        {
            if (!File.Exists(trackFile))
            {
                return;
            }

            audioFile = new AudioFileReader(trackFile);
            outputDevice = new WaveOutEvent();
            
            audioFile.Position = 0;
        }

        public void Play(string path)
        {
            if(!File.Exists(path))
            {
                return;
            }

            if (IsPlaying)
            {
                Stop();
            }
            
            Init(path);
            outputDevice.Init(audioFile);
            outputDevice.Play();
            IsPlaying = true;
        }

        public void Stop()
        {
            if (IsPlaying)
            {
                outputDevice?.Stop();
                outputDevice?.Dispose();
                audioFile?.Dispose();
                outputDevice = null;
                audioFile = null;
            }
        }
    }
}
