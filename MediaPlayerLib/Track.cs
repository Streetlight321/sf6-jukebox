﻿using MediaPlayerDAL;

namespace MediaPlayerLib
{
    public class Track
    {
        public Track(string path, FlowSceneId flowSceneId)
        {
            Path = path;
            FlowSceneId = flowSceneId;
            PlaylistType = PlaylistType.FlowScene;
        }

        public Track(string path, StageId stageId)
        {
            Path = path;
            StageId = stageId;
            PlaylistType = PlaylistType.Stage;
        }

        public Track(string path, CustomScreenId customScreenId)
        {
            Path = path;
            CustomScreenId = customScreenId;
            PlaylistType = PlaylistType.CustomScreen;
        }

        public Track(string path, CharacterId characterId)
        {
            Path = path;
            CharacterId = characterId;
            PlaylistType = PlaylistType.Character;
        }

        public string Path { get; }
        public PlaylistType PlaylistType { get; }        
        public FlowSceneId FlowSceneId { get; }
        public StageId StageId { get; }
        public CustomScreenId CustomScreenId { get; }   
        public CharacterId CharacterId { get; }
    }
}
