﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public class LogEventArgs : EventArgs
    {
        public LogEventArgs(string message)
        {
            Message = message;
        }

        public string Message { get; }
    }

    public class VolumeChangedArgs : EventArgs
    {
        public VolumeChangedArgs(float volume)
        {
            Volume = volume;
        }

        public float Volume { get; }
    }
}
