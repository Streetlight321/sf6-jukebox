﻿namespace MediaPlayerLib
{
    public abstract class PlaybackRequest
    {
        public virtual string Name { get; }
    }
}
