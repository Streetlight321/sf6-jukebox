﻿using MediaPlayerDAL.Model;

namespace MediaPlayerDAL.Contract
{
    public interface IScreen
    {
        int Id { get; set; }
        string Name { get; set; }
        Playlist Playlist { get; set; }
        int IsRandomPlaylist { get; set; }
    }
}
