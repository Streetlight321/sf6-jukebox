﻿using MediaPlayerDAL.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
namespace MediaPlayerDAL.Service
{
    public class SerializationService
    {
        public static string LatestVersion { get => "1.25.2"; }
        public string SettingsFilePath 
        {
            get
            {
                var executingDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
                return Path.Combine(executingDir.Parent.FullName, @"plugins\settings.xml"); ;
            }
        }

        public Version GetSettingsVersion()
        {
            Version version = Version.Unknown;
            if (File.Exists(SettingsFilePath))
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                    using (var fs = new FileStream(SettingsFilePath, FileMode.Open))
                    {
                        var settings = x.Deserialize(fs) as Settings;
                        switch (settings.Version) 
                        {
                            case "":
                                version = Version.v_pre_1_21_0;
                                break;
                            case "1.21.0":
                                version = Version.v_1_21_0;
                                break;
                            case "1.22.0":
                                version = Version.v_1_22_0;
                                break;
                            case "1.23.0":
                                version = Version.v_1_23_0;
                                break;
                            case "1.24.0":
                                version = Version.v_1_24_0;
                                break;
                            case "1.25.0":
                                version = Version.v_1_25_0;
                                break;
                            case "1.25.1":
                                version = Version.v_1_25_1;
                                break;
                            case "1.25.2":
                                version = Version.v_1_25_2;
                                break;
                        }
                    }
                }
                catch (Exception ex) { }
            }

            return version;
        }

        public Settings Load()
        {
            Settings settings = GetDefaultSettings();
            if (File.Exists(SettingsFilePath))
            {
                try
                {
                    using (var fs = new FileStream(SettingsFilePath, FileMode.Open))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                        settings = xmlSerializer.Deserialize(fs) as Settings;
                    }

                    var settingsVersion = GetSettingsVersion();
                    switch (settingsVersion)
                    {
                        case Version.v_pre_1_21_0:
                            settings.Shortcuts = GetDefaultKeyBindings();
                            settings.Stages = GetDefaultStages();
                            settings.FlowScenes = GetDefaultFlowScenes();
                            settings.Volume = GetDefaultVolume();
                            settings.Shortcuts.Restart = GetRestartShortcut();
                            settings.Shortcuts.ShowOverlay = GetDefaultOverlayShortcut();
                            settings.OverlayEnabled = GetDefaultOverlayEnabled();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
                            settings.Version = LatestVersion;                            
                            break;

                        case Version.v_1_21_0:
                            settings.FlowScenes = GetDefaultFlowScenes();
                            settings.CustomScreens = GetDefaultCustomScreens();
                            settings.Volume = GetDefaultVolume();
                            settings.Shortcuts.Restart = GetRestartShortcut();
                            settings.Shortcuts.ShowOverlay = GetDefaultOverlayShortcut();
                            settings.OverlayEnabled = GetDefaultOverlayEnabled();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
                            settings.Version = LatestVersion;
                            break;

                        case Version.v_1_22_0:
                            settings.FlowScenes.Insert(6, new FlowScene { Name = "Custom Room", FlowSceneId = FlowSceneId.eCustomRoomMain });
                            settings.CustomScreens = GetDefaultCustomScreens();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            Versioning.v1_22_0.Settings settings_v1_22_0 = LoadSettings_v_1_22_0();
                            foreach(var stage in settings_v1_22_0.Stages)
                            {
                                var newStage = settings.Stages.Single(s => s.Id == stage.Id);                                
                                newStage.Playlist.Tracks.AddRange(stage.Tracks.Select(t=>new Track() { Path = t.Path }));
                            }
                            foreach (var flowScene in settings_v1_22_0.FlowScenes)
                            {
                                var newFlowScene = settings.FlowScenes.Single(fs => fs.Id == (int)flowScene.Id);
                                newFlowScene.Playlist.Tracks.AddRange(flowScene.Tracks.Select(t => new Track() { Path = t.Path }));
                            }
                            settings.Volume = GetDefaultVolume();
                            settings.Shortcuts.Restart = GetRestartShortcut();
                            settings.Shortcuts.ShowOverlay = GetDefaultOverlayShortcut();
                            settings.OverlayEnabled = GetDefaultOverlayEnabled();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
                            settings.Version = LatestVersion;
                            break;

                        case Version.v_1_23_0:
                            settings.FlowScenes.Insert(6, new FlowScene { Name = "Custom Room", FlowSceneId = FlowSceneId.eCustomRoomMain });
                            settings.CustomScreens.AddRange(new List<CustomScreen>
                                {
                                    new CustomScreen { Name = "Kimberly Lvl3", CustomScreenId = CustomScreenId.KimberlyLvl3 },
                                    new CustomScreen { Name = "Kimberly Lvl3 (both)", CustomScreenId = CustomScreenId.KimberlyLvl3Both }
                                });
                            
                            foreach (var stage in settings.Stages)
                            {
                                stage.Playlist.FadeOutDuration = GetStageDefaultFadeout();
                            }
                            foreach (var flowScene in settings.FlowScenes)
                            {
                                flowScene.Playlist.FadeOutDuration = GetFlowSceneDefaultFadeout();
                            }
                            foreach (var customScreen in settings.CustomScreens)
                            {
                                customScreen.Playlist.FadeOutDuration = GetCustomScreenDefaultFadeout();
                            }
                            settings.Shortcuts.Restart = GetRestartShortcut();
                            settings.Shortcuts.ShowOverlay = GetDefaultOverlayShortcut();
                            settings.OverlayEnabled = GetDefaultOverlayEnabled();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
                            settings.Version = LatestVersion;
                            break;

                        case Version.v_1_24_0:
                            settings.Shortcuts.Restart = GetRestartShortcut();
                            settings.Shortcuts.ShowOverlay = GetDefaultOverlayShortcut();
                            settings.OverlayEnabled = GetDefaultOverlayEnabled();
                            settings.Characters = GetDefaultCharacters();
                            settings.PlaylistPriority = GetDefaultPlaylistPriority();
                            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
                            settings.Version = LatestVersion;                        
                            break;

                        case Version.v_1_25_0:
                            settings.Characters = GetDefaultCharacters();
                            settings.Version = LatestVersion;
                            break;
                        case Version.v_1_25_1:
                            settings.Characters = GetDefaultCharacters();
                            settings.Version = LatestVersion;
                            break;
                    }
                } 
                catch 
                {
                    //todo: Find a way to bubble that up to the UI.
                    settings.Version = LatestVersion;
                }
            }

            return settings;
        }

        Versioning.v1_22_0.Settings LoadSettings_v_1_22_0()
        {
            Versioning.v1_22_0.Settings settings = null;
            if (File.Exists(SettingsFilePath))
            {
                try
                {
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(Versioning.v1_22_0.Settings));
                    using (var fileStream = new FileStream(SettingsFilePath, FileMode.Open))
                    {
                        settings = serializer.Deserialize(fileStream) as Versioning.v1_22_0.Settings;
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            return settings;
        }

        public bool Save(Settings settings) 
        {
            if (settings != null)
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(settings.GetType());
                if (File.Exists(SettingsFilePath))
                {
                    File.Delete(SettingsFilePath);
                }

                using (var fs = new FileStream(SettingsFilePath, FileMode.Create))
                {
                    x.Serialize(fs, settings);
                    fs.Flush();
                    return true;
                }
            }

            return false;
        }

        private int GetDefaultOverrideMenusTracks()
        {
            return 0;
        }

        private PlaylistPriority GetDefaultPlaylistPriority()
        {
            return PlaylistPriority.Random;
        }

        private int GetDefaultOverlayEnabled()
        {
            return 1;
        }

        private int GetRestartShortcut()
        {
            return 35;
        }

        private int GetDefaultOverlayShortcut()
        {
            return 192;
        }

        private float GetDefaultVolume()
        {
            return 0.22f;
        }

        private float GetStageDefaultFadeout()
        {
            return 1.0f;
        }

        private float GetCharacterDefaultFadeout()
        {
            return 1.0f;
        }

        private float GetFlowSceneDefaultFadeout()
        {
            return 0.5f;
        }

        private float GetCustomScreenDefaultFadeout()
        {
            return 0;
        }

        private Settings GetDefaultSettings()
        {
            Settings settings = new Settings();

            settings.Version = LatestVersion;
            settings.Enabled = 1;
            settings.OverrideMenusTracks = GetDefaultOverrideMenusTracks();
            settings.PlaylistPriority = PlaylistPriority.Random;
            var executingDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            settings.RandomPlaylistPath = Path.Combine(executingDir.Parent.FullName, @"plugins\CustomTracks");
            settings.Shortcuts = GetDefaultKeyBindings();
            settings.Volume = GetDefaultVolume();
            settings.OverlayEnabled = GetDefaultOverlayEnabled();
            settings.PlaylistPriority = GetDefaultPlaylistPriority();
            settings.Stages = GetDefaultStages();
            settings.FlowScenes = GetDefaultFlowScenes();
            settings.CustomScreens = GetDefaultCustomScreens();
            settings.Characters = GetDefaultCharacters();

            return settings;
        }

        public KeyBindings GetDefaultKeyBindings()
        {
            return new KeyBindings
            {
                NextTrack = 33,
                PreviousTrack = 34,
                VolumeUp = 187,
                VolumeDown = 109,
                Restart = GetRestartShortcut(),
                ShowOverlay = GetDefaultOverlayShortcut()
            };
        }

        public List<Character> GetDefaultCharacters()
        {
            var characters = new List<Character>
            {
                new Character { Name = "Ryu", CharacterId = CharacterId.Ryu },
                new Character { Name = "Luke", CharacterId = CharacterId.Luke },
                new Character { Name = "Kimberly", CharacterId = CharacterId.Kimberly},
                new Character { Name = "ChunLi", CharacterId = CharacterId.ChunLi },
                new Character { Name = "Manon", CharacterId = CharacterId.Manon },
                new Character { Name = "Zangief", CharacterId = CharacterId.Zangief },
                new Character { Name = "JP", CharacterId = CharacterId.JP },
                new Character { Name = "Dhalsim", CharacterId = CharacterId.Dhalsim},
                new Character { Name = "Cammy", CharacterId = CharacterId.Cammy },
                new Character { Name = "Ken", CharacterId = CharacterId.Ken },
                new Character { Name = "DeeJay", CharacterId = CharacterId.DeeJay },
                new Character { Name = "Lily", CharacterId = CharacterId.Lily },
                new Character { Name = "Rashid", CharacterId = CharacterId.Rashid },
                new Character { Name = "Blanka", CharacterId = CharacterId.Blanka },
                new Character { Name = "Marisa", CharacterId = CharacterId.Marisa },
                new Character { Name = "Juri", CharacterId = CharacterId.Juri },
                new Character { Name = "Guile", CharacterId = CharacterId.Guile },
                new Character { Name = "Honda", CharacterId = CharacterId.Honda },
                new Character { Name = "Jamie", CharacterId = CharacterId.Jamie },
                new Character { Name = "Aki", CharacterId = CharacterId.Aki }
            };

            foreach(var character in characters)
            {
                character.IsRandomPlaylist = 1;
                character.Playlist.FadeOutDuration = GetCharacterDefaultFadeout();
            }

            return characters;
        }

        public List<Stage> GetDefaultStages()
        {
            var stages = new List<Stage>
            {
                new Stage { Name = "World Tour", Id=10 },
                new Stage { Name = "Barmaley Steelworks", Id = 50000 },
                new Stage { Name = "Bathers Beach", Id = 90000 },
                new Stage { Name = "Carrier Byron Taylor", Id=40100 },
                new Stage { Name = "Colosseo", Id=120000 },
                new Stage { Name = "Dhalsimer Temple", Id=70000 },
                new Stage { Name = "Fête Foraine", Id=60000 },
                new Stage { Name = "Genbu Temple", Id=20000 },
                new Stage { Name = "King Street", Id=100000 },
                new Stage { Name = "Metro City Downtown", Id=40000 },
                new Stage { Name = "Old Town Market", Id=10000 },
                new Stage { Name = "Ranger's Hut", Id=80000 },
                new Stage { Name = "Suval'hal Arena", Id=10100 },
                new Stage { Name = "The Macho Ring", Id=400000 },
                new Stage { Name = "Thunderfoot Settlement", Id=110000 },
                new Stage { Name = "Tian Hong Yuan", Id=30000 },
                new Stage { Name = "Training Room", Id=0 }
            };

            foreach (var stage in stages)
            {
                stage.IsRandomPlaylist = 1;
                stage.Playlist.FadeOutDuration = GetStageDefaultFadeout();
            }

            return stages;
        }

        public List<FlowScene> GetDefaultFlowScenes()
        {
            var flowScenes = new List<FlowScene>
            {
                new FlowScene { Name = "Title", FlowSceneId = FlowSceneId.eTitle },
                new FlowScene { Name = "Mode Select", FlowSceneId = FlowSceneId.eModeSelect },
                new FlowScene { Name = "ESports Main Menu", FlowSceneId = FlowSceneId.eESportsMainMenu },
                new FlowScene { Name = "Battle Side Select", FlowSceneId = FlowSceneId.eBattleSideSelect },
                new FlowScene { Name = "Battle Stage Select", FlowSceneId = FlowSceneId.eBattleStageSelect },
                new FlowScene { Name = "Battle Fighter Select", FlowSceneId = FlowSceneId.eBattleFighterSelect },
                new FlowScene { Name = "Custom Room", FlowSceneId = FlowSceneId.eCustomRoomMain },
                new FlowScene { Name = "Mode Select Battle Hub", FlowSceneId = FlowSceneId.eModeSelectBattleHub },
                new FlowScene { Name = "Battle Hub Main", FlowSceneId = FlowSceneId.eBattleHubMain },
                new FlowScene { Name = "Mode Select World Tour", FlowSceneId = FlowSceneId.eModeSelectWorldTour },
                new FlowScene { Name = "World Tour City", FlowSceneId = FlowSceneId.eWorldTourCity },
                new FlowScene { Name = "Battle Fighter VS", FlowSceneId = FlowSceneId.eBattleFighterVS },
                new FlowScene { Name = "Battle Fighter Emote", FlowSceneId = FlowSceneId.eBattleFighterEmote },
                new FlowScene { Name = "Versus Rule", FlowSceneId = FlowSceneId.eVersusRule },
                new FlowScene { Name = "Arcade Fighter Select", FlowSceneId = FlowSceneId.eArcadeFighterSelect },
                new FlowScene { Name = "Arcade Comic Demo", FlowSceneId = FlowSceneId.eArcadeComicDemo },
                new FlowScene { Name = "Arcade Stage Move", FlowSceneId = FlowSceneId.eArcadeStageMove },
                new FlowScene { Name = "Avatar Battle", FlowSceneId = FlowSceneId.eWorldTourOnlineBattle },
                new FlowScene { Name = "Avatar Creation", FlowSceneId = FlowSceneId.eBattleHubCharacterCreateShop }
            };

            foreach(var flowScene in flowScenes)
            {
                flowScene.IsRandomPlaylist = 1;
                flowScene.Enabled = 0;
                flowScene.Playlist.FadeOutDuration = GetFlowSceneDefaultFadeout();
            }

            return flowScenes;
        }

        public List<CustomScreen> GetDefaultCustomScreens()
        {
            var customScreens = new List<CustomScreen>
            {
                new CustomScreen {Name = "Result Screen", CustomScreenId = CustomScreenId.ResultScreen},
                new CustomScreen {Name = "Kimberly Lvl3", CustomScreenId = CustomScreenId.KimberlyLvl3},
                new CustomScreen {Name = "Kimberly Lvl3 (both)", CustomScreenId = CustomScreenId.KimberlyLvl3Both}
            };

            foreach (var screen in customScreens)
            {
                screen.Playlist.FadeOutDuration = GetCustomScreenDefaultFadeout();
            }

            return customScreens;
        }
    }
}
