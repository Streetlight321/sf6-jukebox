﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Model
{
    public class Character : BaseScreen
    {
        [XmlIgnore]
        public CharacterId CharacterId { get; set; }

        public override int Id
        {
            get
            {
                return (int)CharacterId;
            }
            set
            {
                CharacterId = (CharacterId)value;
            }
        }
    }
}
