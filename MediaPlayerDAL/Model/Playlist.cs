﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public enum PlayMode
    {
        Repeat,
        Once,
        SingleRepeat,
        SingleOnce
    };

    public class Playlist
    {
        public Playlist()
        {
            Tracks = new List<Track>();
        }

        public PlayMode Mode { get; set; }
        public float FadeOutDuration { get; set; }
        public List<Track> Tracks { get; set; }
    }
}
