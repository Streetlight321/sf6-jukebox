﻿using MediaPlayerDAL.Contract;
using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public abstract class BaseScreen : IScreen
    {
        public BaseScreen()
        {
            Playlist = new Playlist();
        }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int IsRandomPlaylist { get; set; }
        public Playlist Playlist { get; set; }
    }
}
