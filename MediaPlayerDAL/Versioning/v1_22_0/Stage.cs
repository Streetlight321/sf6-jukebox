﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Versioning.v1_22_0
{
    public class Stage
    {
        public Stage()
        {
            Tracks = new List<Track>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public List<Track> Tracks { get; set; }
        public int IsRandomPlaylist { get; set; }
    }
}
