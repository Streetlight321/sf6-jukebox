﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_22_0
{
    public class FlowScene
    {
        public FlowSceneId Id { get; set; }
        public string Name { get; set; }
        public List<Track> Tracks { get; set; }
        public int IsRandomPlaylist { get; set; }
        public int Enabled { get; set; }
    }

    [Serializable]
    public enum FlowSceneId : int
    {
        [XmlEnum("0")]
        eNone = 0,
        [XmlEnum("1")]
        eBoot = 1,
        [XmlEnum("2")]
        eBootSetup = 2,
        [XmlEnum("3")]
        eTitle = 3,
        [XmlEnum("4")]
        eESportsMainMenu = 4,
        [XmlEnum("5")]
        eModeSelect = 5,
        [XmlEnum("6")]
        eReturnModeSelect = 6,
        [XmlEnum("7")]
        eBootedTransitionHub = 7,
        [XmlEnum("8")]
        eBattleSideSelect = 8,
        [XmlEnum("9")]
        eBattleStageSelect = 9,
        [XmlEnum("10")]
        eBattleFighterSelect = 10,
        [XmlEnum("11")]
        eBattleFighterLeave = 11,
        [XmlEnum("12")]
        eBattleFighterVS = 12,
        [XmlEnum("13")]
        eBattleFighterEmote = 13,
        [XmlEnum("14")]
        eBattleMain = 14, // Fight is starting
        [XmlEnum("15")]
        eDeathMatchRuleSelect = 15,
        [XmlEnum("16")]
        eDeathMatchMain = 16,
        [XmlEnum("17")]
        eTeamBattleRuleSelect = 17,
        [XmlEnum("18")]
        eBattleMatchingIn = 18,
        [XmlEnum("19")]
        eBattleMatchingOut = 19,
        [XmlEnum("20")]
        eBattleAssetReload = 20,
        [XmlEnum("21")]
        eTransitionCustomRoom = 21,
        [XmlEnum("22")]
        eCustomRoomSetting = 22,
        [XmlEnum("23")]
        eCustomRoomInit = 23,
        [XmlEnum("24")]
        eCustomRoomInvite = 24,
        [XmlEnum("25")]
        eCustomRoomLogin = 25,
        [XmlEnum("26")]
        eCustomRoomFromBattleResult = 26,
        [XmlEnum("27")]
        eCustomRoomMain = 27,
        [XmlEnum("28")]
        eCustomRoomViewingMain = 28,
        [XmlEnum("29")]
        eTeamBattleMatchingIn = 29,
        [XmlEnum("30")]
        eExamCpuBattleSetup = 30,
        [XmlEnum("31")]
        eReadyAsset = 31,
        [XmlEnum("32")]
        eTrainingSetup = 32,
        [XmlEnum("33")]
        eOnlineTrainingSetup = 33,
        [XmlEnum("34")]
        eTutorialSetup = 34,
        [XmlEnum("35")]
        eTutorialSelect = 35,
        [XmlEnum("36")]
        eCharacterGuideSetup = 36,
        [XmlEnum("37")]
        eCharacterGuideSelect = 37,
        [XmlEnum("38")]
        eComboTrialSetup = 38,
        [XmlEnum("39")]
        eComboTrialSelect = 39,
        [XmlEnum("40")]
        eReplaySetup = 40,
        [XmlEnum("41")]
        eFGReplaySetup = 41,
        [XmlEnum("42")]
        eVersusBackReplay = 42,
        [XmlEnum("43")]
        eSpectateSetup = 43,
        [XmlEnum("44")]
        eSpectateAssetReload = 44,
        [XmlEnum("45")]
        eVersusRule = 45,
        [XmlEnum("46")]
        eModeSelectBattleHub = 46,
        [XmlEnum("47")]
        eModeSelectWorldTour = 47,
        [XmlEnum("48")]
        eModeSelectActivity = 48,
        [XmlEnum("49")]
        eDeleteSaveDataWorldTour = 49,
        [XmlEnum("50")]
        eWorldTourOpening = 50,
        [XmlEnum("51")]
        eWorldTourContinue = 51,
        [XmlEnum("52")]
        eWorldTourCharacterCreateOpening = 52,
        [XmlEnum("53")]
        eWorldTourCharacterCreateShop = 53,
        [XmlEnum("54")]
        eWorldTourCity = 54,
        [XmlEnum("55")]
        eWorldTourEngageBattleReady = 55,
        [XmlEnum("56")]
        eWorldTourEngageBattleOut = 56,
        [XmlEnum("57")]
        eWorldTourMatchUpBattleReady = 57,
        [XmlEnum("58")]
        eWorldTourMatchUpBattleOut = 58,
        [XmlEnum("59")]
        eWorldTourBattle = 59,
        [XmlEnum("60")]
        eWorldTourBattleSound = 60,
        [XmlEnum("61")]
        eWorldTourMiniGame = 61,
        [XmlEnum("62")]
        eWorldTourMiniGameBattle = 62,
        [XmlEnum("63")]
        eWorldTourTutorial = 63,
        [XmlEnum("64")]
        eWorldTourTutorialBattle = 64,
        [XmlEnum("65")]
        eWorldTourOnlineBattle = 65,
        [XmlEnum("66")]
        eWorldTourSpectateReady = 66,
        [XmlEnum("67")]
        eWorldTourTrainingReady = 67,
        [XmlEnum("68")]
        eBattleHubInvite = 68,
        [XmlEnum("69")]
        eBattleHubInit = 69,
        [XmlEnum("70")]
        eBattleHubCheckAdmissionEvent = 70,
        [XmlEnum("71")]
        eBattleHubLogin = 71,
        [XmlEnum("72")]
        eBattleHubMain = 72,
        [XmlEnum("73")]
        eBattleHubAvatarBattleIn = 73,
        [XmlEnum("74")]
        eBattleHubAvatarBattleOut = 74,
        [XmlEnum("75")]
        eBattleHubCharacterCreateOpening = 75,
        [XmlEnum("76")]
        eBattleHubCharacterCreateShop = 76,
        [XmlEnum("77")]
        eBattleHubIntroduction = 77,
        [XmlEnum("78")]
        eBattleHubPlayContact = 78,
        [XmlEnum("79")]
        eArcadeFighterSelect = 79,
        [XmlEnum("80")]
        eArcadeComicDemo = 80,
        [XmlEnum("81")]
        eArcadeStageMove = 81,
        [XmlEnum("82")]
        eArcadeResult = 82,
        [XmlEnum("83")]
        eArcadeContinue = 83,
        [XmlEnum("84")]
        eArcadeStaffRoll = 84,
        [XmlEnum("85")]
        eArcadeEndCard = 85,
        [XmlEnum("86")]
        eArcadePlayThanks = 86,
        [XmlEnum("87")]
        eLogin = 87,
        [XmlEnum("88")]
        eGalleryTop = 88,
        [XmlEnum("89")]
        eGalleryEventCmn = 89,
        [XmlEnum("90")]
        eGalleryEventWT = 90,
        [XmlEnum("91")]
        eGalleryEventBH = 91,
        [XmlEnum("92")]
        eGalleryEventFG = 92,
        [XmlEnum("93")]
        eMameMain = 93,
        [XmlEnum("94")]
        eMameSpectate = 94,
        [XmlEnum("95")]
        eEsfCmn00 = 95,
        [XmlEnum("96")]
        eWtfCmn00 = 96,
        [XmlEnum("97")]
        eCntVersusDemo = 97,
        [XmlEnum("98")]
        eCommentatorResource = 98,
        [XmlEnum("99")]
        eCommentatorResourceWT = 99,
        [XmlEnum("100")]
        eFGTutorialResource = 100,
        [XmlEnum("101")]
        eFGComboTrialResource = 101,
        [XmlEnum("102")]
        eFGCharacterGuideResource = 102,
        [XmlEnum("103")]
        eCtrlBattleFlowSound = 103,
        [XmlEnum("104")]
        eCommentatorSystem = 104,
        [XmlEnum("105")]
        eCommentatorSystemWT = 105,
        [XmlEnum("106")]
        eReturnHome = 106,
        [XmlEnum("107")]
        eFlowMain = 107,
        [XmlEnum("108")]
        eInvite = 108,
        [XmlEnum("109")]
        eTransitionActivityFlow = 109,
        [XmlEnum("110")]
        eTransitionFlowMain = 110,
        [XmlEnum("111")]
        eResidentData = 111,
        [XmlEnum("112")]
        eResident = 112
    };
}
