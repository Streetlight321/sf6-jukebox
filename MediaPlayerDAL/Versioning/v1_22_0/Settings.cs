﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Versioning.v1_22_0
{
    public class Settings
    {
        public string Version { get; set; }
        public int Enabled { get; set; }
        public string RandomPlaylistPath { get; set; }
        public KeyBindings Shortcuts { get; set; }
        public List<Stage> Stages { get; set; }
        public List<FlowScene> FlowScenes { get; set; }
    }
}
