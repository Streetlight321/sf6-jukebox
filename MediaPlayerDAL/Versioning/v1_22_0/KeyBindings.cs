﻿namespace MediaPlayerDAL.Versioning.v1_22_0
{
    public class KeyBindings
    {
        public int NextTrack { get; set; }
        public int PreviousTrack { get; set; }
        public int VolumeUp { get; set; }
        public int VolumeDown { get; set; }
    }
}
