﻿using System;
using System.Xml.Serialization;

namespace MediaPlayerDAL
{
    public enum Version
    {
        Unknown,
        v_pre_1_21_0,
        v_1_21_0,
        v_1_22_0,
        v_1_23_0,
        v_1_24_0,
        v_1_25_0,
        v_1_25_1,
        v_1_25_2
    }

    [Serializable]
    public enum PlaylistPriority
    {
        [XmlEnum("0")]
        Random = 0,
        [XmlEnum("1")]
        Stage = 1,
        [XmlEnum("2")]
        P1 = 2,
        [XmlEnum("3")]
        P2 = 3,
        [XmlEnum("4")]
        Player = 4,
        [XmlEnum("5")]
        Opponent = 5        
    }        

    public enum CharacterId : int
    {
        NotSet = 0,
        Ryu = 1,
        Luke = 2,
        Kimberly = 3,
        ChunLi = 4,
        Manon = 5,
        Zangief = 6,
        JP = 7,
        Dhalsim = 8,
        Cammy = 9,
        Ken = 10,
        DeeJay = 11,
        Lily = 12,
        Aki = 13,
        Rashid = 14,
        Blanka = 15,
        Juri = 16,
        Marisa = 17,
        Guile = 18,
        Honda = 20,
        Jamie = 21,
        PlayerType_Unknown = -1
    };

    public enum CustomScreenId : int
    {
        ResultScreen = 0,
        KimberlyLvl3 = 1,
        KimberlyLvl3Both = 2,
        Unknown = -1,
    }

    [Serializable]
    public enum StageId : int
    {
        WorldTour = 10,
        BarmaleySteelworks = 50000,
        BathersBeach = 90000,
        CarrierByronTaylor = 40100,
        Colosseo = 120000,
        DhalsimerTemple = 70000,
        FeteForaine = 60000,
        GenbuTemple = 20000,
        KingStreet = 100000,
        MetroCityDowntown = 40000,
        OldTownMarket = 10000,
        RangersHut = 80000,
        SuvalhalArena = 10100,
        TheMachoRing = 400000,
        ThunderfootSettlement = 110000,
        TianHongYuan = 30000,
        TrainingRoom = 0,
        Unknown = -1,
    }

    [Serializable]
    public enum FlowSceneId : int
    {
        eNone = 0,
        eBoot = 1,
        eBootSetup = 2,
        eTitle = 3,
        eESportsMainMenu = 4,
        eModeSelect = 5,
        eReturnModeSelect = 6,
        eBootedTransitionHub = 7,
        eBattleSideSelect = 8,
        eBattleStageSelect = 9,
        eBattleFighterSelect = 10,
        eBattleFighterLeave = 11,
        eBattleFighterVS = 12,
        eBattleFighterEmote = 13,
        eBattleMain = 14, // Fight is starting
        eDeathMatchRuleSelect = 15,
        eDeathMatchMain = 16,
        eTeamBattleRuleSelect = 17,
        eBattleMatchingIn = 18,
        eBattleMatchingOut = 19,
        eBattleAssetReload = 20,
        eTransitionCustomRoom = 21,
        eCustomRoomSetting = 22,
        eCustomRoomInit = 23,
        eCustomRoomInvite = 24,
        eCustomRoomLogin = 25,
        eCustomRoomFromBattleResult = 26,
        eCustomRoomMain = 27,
        eCustomRoomViewingMain = 28,
        eTeamBattleMatchingIn = 29,
        eExamCpuBattleSetup = 30,
        eReadyAsset = 31,
        eTrainingSetup = 32,
        eOnlineTrainingSetup = 33,
        eTutorialSetup = 34,
        eTutorialSelect = 35,
        eCharacterGuideSetup = 36,
        eCharacterGuideSelect = 37,
        eComboTrialSetup = 38,
        eComboTrialSelect = 39,
        eReplaySetup = 40,
        eFGReplaySetup = 41,
        eVersusBackReplay = 42,
        eSpectateSetup = 43,
        eSpectateAssetReload = 44,
        eVersusRule = 45,
        eModeSelectBattleHub = 46,
        eModeSelectWorldTour = 47,
        eModeSelectActivity = 48,
        eDeleteSaveDataWorldTour = 49,
        eWorldTourOpening = 50,
        eWorldTourContinue = 51,
        eWorldTourCharacterCreateOpening = 52,
        eWorldTourCharacterCreateShop = 53,
        eWorldTourCity = 54,
        eWorldTourEngageBattleReady = 55,
        eWorldTourEngageBattleOut = 56,
        eWorldTourMatchUpBattleReady = 57,
        eWorldTourMatchUpBattleOut = 58,
        eWorldTourBattle = 59,
        eWorldTourBattleSound = 60,
        eWorldTourMiniGame = 61,
        eWorldTourMiniGameBattle = 62,
        eWorldTourTutorial = 63,
        eWorldTourTutorialBattle = 64,
        eWorldTourOnlineBattle = 65,
        eWorldTourSpectateReady = 66,
        eWorldTourTrainingReady = 67,
        eBattleHubInvite = 68,
        eBattleHubInit = 69,
        eBattleHubCheckAdmissionEvent = 70,
        eBattleHubLogin = 71,
        eBattleHubMain = 72,
        eBattleHubAvatarBattleIn = 73,
        eBattleHubAvatarBattleOut = 74,
        eBattleHubCharacterCreateOpening = 75,
        eBattleHubCharacterCreateShop = 76,
        eBattleHubIntroduction = 77,
        eBattleHubPlayContact = 78,
        eArcadeFighterSelect = 79,
        eArcadeComicDemo = 80,
        eArcadeStageMove = 81,
        eArcadeResult = 82,
        eArcadeContinue = 83,
        eArcadeStaffRoll = 84,
        eArcadeEndCard = 85,
        eArcadePlayThanks = 86,
        eLogin = 87,
        eGalleryTop = 88,
        eGalleryEventCmn = 89,
        eGalleryEventWT = 90,
        eGalleryEventBH = 91,
        eGalleryEventFG = 92,
        eMameMain = 93,
        eMameSpectate = 94,
        eEsfCmn00 = 95,
        eWtfCmn00 = 96,
        eCntVersusDemo = 97,
        eCommentatorResource = 98,
        eCommentatorResourceWT = 99,
        eFGTutorialResource = 100,
        eFGComboTrialResource = 101,
        eFGCharacterGuideResource = 102,
        eCtrlBattleFlowSound = 103,
        eCommentatorSystem = 104,
        eCommentatorSystemWT = 105,
        eReturnHome = 106,
        eFlowMain = 107,
        eInvite = 108,
        eTransitionActivityFlow = 109,
        eTransitionFlowMain = 110,
        eResidentData = 111,
        eResident = 112
    };
}
