﻿using MediaPlayerDAL.Service;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using PlaylistManager.View;
using PlaylistManager.ViewModel;
using System.Windows;

namespace PlaylistManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
            Title = $"{Title} - v{SerializationService.LatestVersion}";
            EventAggregator.Subscribe<MessageBoxEvent>(OnMessageBoxEvent);
        }

        private void OnMessageBoxEvent(MessageBoxEvent messageBoxEvent)
        {
            var messageBoxView = new MessageBoxView();
            var vm = new MessageBoxViewModel();
            vm.Title = messageBoxEvent.Title;
            vm.Message = messageBoxEvent.Message;
            messageBoxView.DataContext = vm;
            messageBoxView.Owner = Application.Current.MainWindow;
            messageBoxView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            messageBoxView.ShowDialog();
        }
    }
}
