﻿using PlaylistManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlaylistManager.View
{
    /// <summary>
    /// Interaction logic for CustomScreenView.xaml
    /// </summary>
    public partial class CustomScreenView : UserControl
    {
        public CustomScreenView()
        {
            InitializeComponent();
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                var vm = DataContext as CustomScreenViewModel;
                if(vm!= null)
                {
                    vm.AddTracksToPlaylist(files.Where(f => f.ToLower().EndsWith(".mp3")).ToList());
                }
            }
        }

        private void ListView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                var vm = DataContext as CustomScreenViewModel;
                if (vm != null)
                {
                    vm.RemoveSelectedTracks();
                }
            }
        }
    }
}
