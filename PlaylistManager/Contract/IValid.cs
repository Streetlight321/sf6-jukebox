﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistManager.Contract
{
    public interface IValid
    {
        bool IsValid { get; }
    }
}
