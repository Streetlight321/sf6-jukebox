﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;
using MediaPlayerDAL.Service;
using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using PlaylistManager.View;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace PlaylistManager.ViewModel
{
    internal class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel()
        {
            PlayerService = new PlayerService();
            SerializationService = new SerializationService();
            SaveCommand = new RelayCommand(OnSave);
            Stages = new PlaylistsContainerViewModel<StageViewModel>("Stages");
            FlowScenes = new PlaylistsContainerViewModel<FlowSceneViewModel>("Menus");
            CustomScreens = new PlaylistsContainerViewModel<CustomScreenViewModel>("Misc");
            Characters = new PlaylistsContainerViewModel<CharacterViewModel>("Characters");
            List<IValid> containers = new List<IValid>();
            containers.Add(Stages);
            containers.Add(FlowScenes);
            containers.Add(CustomScreens);
            containers.Add(Characters);
            Notification = new NotificationViewModel(containers);
            Load();
        }

        private void Load()
        {
            var settings = SerializationService.Load();
            Settings = new SettingsViewModel(settings);
            foreach (var stage in settings.Stages)
            {
                Stages.Playlists.Add(new StageViewModel(stage, PlayerService));
            }

            foreach(var flowScene in settings.FlowScenes)
            {
                FlowScenes.Playlists.Add(new FlowSceneViewModel(flowScene, PlayerService));
            }

            foreach(var customScreen in settings.CustomScreens) 
            { 
                CustomScreens.Playlists.Add(new CustomScreenViewModel(customScreen, PlayerService));
            }

            foreach (var characters in settings.Characters)
            {
                Characters.Playlists.Add(new CharacterViewModel(characters, PlayerService));
            }
        }

        public NotificationViewModel Notification { get; set; }
        public PlaylistsContainerViewModel<StageViewModel> Stages { get; set; }
        public PlaylistsContainerViewModel<FlowSceneViewModel> FlowScenes { get; set; }
        public PlaylistsContainerViewModel<CustomScreenViewModel> CustomScreens { get; set; }
        public PlaylistsContainerViewModel<CharacterViewModel> Characters { get; set; }
        public SettingsViewModel Settings { get; set; }
        public PlayerService PlayerService { get; }
        public SerializationService SerializationService {get;}
        public RelayCommand SaveCommand { get; set; }

        private void OnSave(object obj)
        {
            Settings settings = new Settings();
            settings.Enabled = Settings.IsEnabled ? 1 : 0;
            settings.RandomPlaylistPath = Settings.RandomPlaylistPath;
            settings.Volume = Settings.Volume;
            settings.OverlayEnabled = Settings.IsOverlayEnabled ? 1 : 0;
            settings.PlaylistPriority = Settings.SelectedPlaylistPriority.Model;
            settings.OverrideMenusTracks = Settings.OverrideMenusTracks ? 1 : 0;
            settings.Shortcuts = Settings.Shortcuts.GetModel();
            settings.Stages = new List<Stage>();
            settings.FlowScenes = new List<FlowScene>();
            settings.CustomScreens = new List<CustomScreen>();
            settings.Characters = new List<Character>();
            settings.Version = Settings.Version;            

            foreach (var stage in Stages.Playlists)
            {
                settings.Stages.Add(stage.ToModel());                
            }

            foreach(var flowScene in FlowScenes.Playlists)
            {
                settings.FlowScenes.Add(flowScene.ToModel());
            }

            foreach (var customScreen in CustomScreens.Playlists)
            {
                settings.CustomScreens.Add(customScreen.ToModel());                
            }

            foreach (var character in Characters.Playlists)
            {
                settings.Characters.Add(character.ToModel());
            }

            if (SerializationService.Save(settings))
            {
                EventAggregator.Publish(new MessageBoxEvent("Settings saved!", "Success"));
            }
            else
            {
                EventAggregator.Publish(new MessageBoxEvent("Failed to save settings.", "Error"));
            }
        }
    }
}
