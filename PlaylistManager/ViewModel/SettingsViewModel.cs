﻿using MediaPlayerDAL.Model;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace PlaylistManager.ViewModel
{
    public class SettingsViewModel : BaseViewModel
    {
        private string randomPlaylistPath;
        private bool overrideMenusTracks;

        public SettingsViewModel(Settings settings)
        {
            IsEnabled = settings.Enabled == 1;
            RandomPlaylistPath = settings.RandomPlaylistPath;
            Shortcuts = KeyBindingsViewModel.FromModel(settings.Shortcuts);
            Version = settings.Version;    
            Volume = settings.Volume;
            OverrideMenusTracks = settings.OverrideMenusTracks == 1;
            PlaylistPriorities = new ObservableCollection<PlaylistPriorityViewModel>
            {
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.Random),
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.Stage),
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.P1),
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.P2),
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.Player),
                new PlaylistPriorityViewModel(MediaPlayerDAL.PlaylistPriority.Opponent)
            };
            SelectedPlaylistPriority = PlaylistPriorities.Single(p => p.Model == settings.PlaylistPriority);
            IsOverlayEnabled = settings.OverlayEnabled == 1;
            OpenRandomTrackFolderCommand = new RelayCommand(OnOpenRandomTrackFolderCommand, CanExecuteOpenRandomTrackFolderCommand);
        }

        public KeyBindingsViewModel Shortcuts { get; set; }
        public string Version { get; set; }
        public float Volume { get; set; }
        public bool IsOverlayEnabled { get; set; }
        public bool IsEnabled { get; set; }
        public RelayCommand OpenRandomTrackFolderCommand { get; set; }
        public ObservableCollection<PlaylistPriorityViewModel> PlaylistPriorities { get; set; }
        public PlaylistPriorityViewModel SelectedPlaylistPriority { get; set; }

        private bool CanExecuteOpenRandomTrackFolderCommand(object obj)
        {
            return Directory.Exists(RandomPlaylistPath);
        }

        private void OnOpenRandomTrackFolderCommand(object obj)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                Arguments = RandomPlaylistPath,
                FileName = "explorer.exe"
            };

            Process.Start(startInfo);
        }

        public void SetNextTrackKey(Key key)
        {
            Shortcuts.NextTrack = key;
        }

        public void SetPreviousTrackKey(Key key)
        {
            Shortcuts.PreviousTrack = key;
        }

        public void SetVolumeUpKey(Key key)
        {
            Shortcuts.VolumeUp = key;
        }

        public void SetVolumeDownKey(Key key)
        {
            Shortcuts.VolumeDown = key;
        }

        public void SetShowOverlayKey(Key key)
        {
            Shortcuts.ShowOverlay = key;
        }

        public void SetRestartKey(Key key)
        {
            Shortcuts.Restart = key;
        }

        public string RandomPlaylistPath 
        { 
            get => randomPlaylistPath;
            set
            {
                randomPlaylistPath = value;
                OnPropertyChanged();
            }
        }

        public bool OverrideMenusTracks
        {
            get => overrideMenusTracks;
            set
            {
                overrideMenusTracks = value;
                OnPropertyChanged();
            }
        }
    }
}
