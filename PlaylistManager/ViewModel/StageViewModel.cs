﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    internal class StageViewModel : BaseScreenViewModel<Stage>
    {
        public StageViewModel(Stage model, PlayerService playerService) 
            : base(model, playerService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Stages/{0}.png", Name);
    }
}
