﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    internal class CharacterViewModel : BaseScreenViewModel<Character>
    {
        public CharacterViewModel(Character model, PlayerService playerService) : base(model, playerService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Characters/{0}.png", Model.CharacterId);
    }
}
