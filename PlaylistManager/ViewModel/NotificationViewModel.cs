﻿using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System.Collections.Generic;

namespace PlaylistManager.ViewModel
{
    public class NotificationViewModel : BaseViewModel
    {
        public NotificationViewModel(List<IValid> playlistsContainers)
        {
            IsVisibile = false;
            EventAggregator.Subscribe<PlaylistValidityUpdatedEvent>(OnPlaylistValidityUpdated);
            PlaylistsContainers = playlistsContainers;
        }

        private bool isVisibile;

        private List<IValid> PlaylistsContainers { get; }
        public string Description { get; set; }

        public bool IsVisibile
        {
            get => isVisibile;
            set
            {
                isVisibile = value;
                OnPropertyChanged();
            }
        }

        private void UpdateVisibility()
        {
            IsVisibile = false;
            foreach (var playlistContainer in PlaylistsContainers)
            {
                IsVisibile |= !playlistContainer.IsValid;
            }

            if(IsVisibile)
            {
                Description = "You have one or more playlist(s) in an invalid state.";
            }
        }

        private void OnPlaylistValidityUpdated(PlaylistValidityUpdatedEvent e)
        {
            UpdateVisibility();
        }
    }
}
