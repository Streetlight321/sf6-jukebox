﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;
using System.Collections.Generic;

namespace PlaylistManager.ViewModel
{
    public class FlowSceneViewModel : BaseScreenViewModel<FlowScene>
    {
        private bool isEnabled;

        public FlowSceneViewModel(FlowScene flowScene, PlayerService playerService)
            : base(flowScene, playerService)
        {
            IsEnabled = flowScene.Enabled == 1;            
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Menus/{0}.png", Model.FlowSceneId);
        public bool IsEnabled
        {
            get => isEnabled; 
            set
            {
                isEnabled = value;
                OnPropertyChanged();
            }
        }

        public override FlowScene ToModel()
        {
            var model = base.ToModel();
            model.Enabled = IsEnabled ? 1 : 0;
            return model;
        }

        internal override void AddTracksToPlaylist(List<string> tracks)
        {
            base.AddTracksToPlaylist(tracks);
            IsEnabled = true;
        }
    }
}
