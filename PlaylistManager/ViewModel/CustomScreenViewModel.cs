﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    public class CustomScreenViewModel : BaseScreenViewModel<CustomScreen>
    {
        public CustomScreenViewModel(CustomScreen model, PlayerService playerService) : base(model, playerService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/CustomScreens/{0}.png", Model.CustomScreenId);
    }
}
