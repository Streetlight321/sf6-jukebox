﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;
using System.IO;

namespace PlaylistManager.ViewModel
{
    public class TrackViewModel : BaseViewModel
    {
        public TrackViewModel(Track track, PlayerService playerService)
        {
            Track = track;
            FullPath = Track.Path;
            IsPathValid = File.Exists(FullPath);
            PlayerService = playerService;
            PlayerService.OnPlay += PlayerService_OnPlay;
            PlayCommand = new RelayCommand(x => 
            {
                PlayerService.Play(FullPath);
                IsPlaying = true;
            }, y => IsPathValid);

            StopCommand = new RelayCommand(x =>
            {
                PlayerService.Stop();
                IsPlaying = false;
            });
        }

        public bool IsPathValid 
        { 
            get;
        }

        private void PlayerService_OnPlay(object? sender, string e)
        {
            if(IsPlaying && FullPath != e)
            {
                PlayerService.Stop();
                IsPlaying= false;
            }
        }

        public Track GetModel()
        {
            Track.Path = FullPath;
            return Track;
        }

        private Track Track { get; }
        PlayerService PlayerService { get; set; }

        private bool isSelected;
        private bool isPlaying;

        public int Id { get; set; }
        public string FileName => String.IsNullOrEmpty(FullPath) ? String.Empty : Path.GetFileName(FullPath);
        public string FullPath { get; set; }
        public RelayCommand PlayCommand { get; set; }
        public RelayCommand StopCommand { get; set; }
        public bool IsPlaying 
        { 
            get => isPlaying; 
            set
            {
                isPlaying = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected 
        {
            get => isSelected; 
            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }
    }
}
