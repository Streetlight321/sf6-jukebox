﻿using PlaylistManager.Service;
using System.Collections.ObjectModel;
using PlaylistManager.Contract;
using MediaPlayerDAL.Contract;
using System.Linq;
using Microsoft.Win32;
using System.Windows;
using System.Collections.Generic;
using MediaPlayerDAL.Model;
using PlaylistManager.GlobalEvent;

namespace PlaylistManager.ViewModel
{
    public abstract class BaseScreenViewModel<T> : BaseViewModel, IValid, IScreenViewModel<T> where T : IScreen
    {
        private bool isRandomPlaylist;
        private bool isValid;

        public BaseScreenViewModel(T model, PlayerService playerService)
        {
            Model = model;
            PlayerService = playerService;
            Playlist = new ObservableCollection<TrackViewModel>();
            foreach(var track in Model.Playlist.Tracks)
            {
                Playlist.Add(new TrackViewModel(track, PlayerService));
            }

            IsRandomPlaylist = Model.IsRandomPlaylist == 1;
            AddTrackCommand = new RelayCommand(OnAddTrackCommand);
            RemoveTrackCommand = new RelayCommand(OnRemoveTrackCommand, x => { return Playlist.Any(t => t.IsSelected); });
            Playlist.CollectionChanged += Playlist_CollectionChanged;
            ValidatePlaylist();
        }

        public ObservableCollection<TrackViewModel> Playlist { get; set; }
        public T Model { get; }
        public PlayerService PlayerService { get; }
        public RelayCommand AddTrackCommand { get; set; }
        public RelayCommand RemoveTrackCommand { get; set; }
        public abstract string ThumbnailPath { get; }
        public PlayMode CurrentMode { get => Model.Playlist.Mode; set => Model.Playlist.Mode = value; }
        public float FadeOutDuration { get => Model.Playlist.FadeOutDuration; set => Model.Playlist.FadeOutDuration = value; }
        public ObservableCollection<TrackViewModel> SelectedTracks { get; set; }
        public bool IsValid 
        { 
            get => isValid; 
            private set
            {
                bool wasUpdated = isValid != value;
                isValid = value;
                OnPropertyChanged();
                if(wasUpdated)
                {
                    EventAggregator.Publish(new PlaylistValidityUpdatedEvent(this));
                }
            }

        }

        public string Name => Model.Name;

        private void Playlist_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ValidatePlaylist();
            if(!Playlist.Any())
            {
                IsRandomPlaylist = true;
            }
        }

        private void ValidatePlaylist()
        {
            IsValid = true;
            foreach (var track in Playlist)
            {
                IsValid &= track.IsPathValid;
            }
        }

        public bool IsRandomPlaylist 
        {
            get => isRandomPlaylist; 
            set
            {
                isRandomPlaylist = value;
                OnPropertyChanged();
            }
        }

        private void OnRemoveTrackCommand(object obj)
        {
            for (int i = 0; i < Playlist.Count; i++)
            {
                if (Playlist[i].IsSelected)
                {
                    Playlist.RemoveAt(i);
                    i--;
                }
            }
        }

        public void RemoveSelectedTracks()
        {            
            for(int i = 0; i < Playlist.Count;i++)
            {
                if (Playlist[i].IsSelected)
                {
                    Playlist.RemoveAt(i--);
                }
            }
        }

        private void OnAddTrackCommand(object obj)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "MP3 file|*.mp3";
            dialog.Multiselect = true;
            bool? result = dialog.ShowDialog();
            if (result != null && result.Value)
            {
                if (Playlist.Any(t => t.FullPath == dialog.FileName))
                {
                    MessageBox.Show("Track already added to this playlist", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    AddTracksToPlaylist(dialog.FileNames.ToList());
                }
            }
        }

        internal virtual void AddTracksToPlaylist(List<string> tracks)
        {
            if(tracks.Any())
            {
                IsRandomPlaylist = false;
            }

            foreach (string track in tracks)
            {
                Playlist.Add(new TrackViewModel(new Track() { Path = track }, PlayerService));
            }
        }

        public virtual T ToModel()
        {
            Model.IsRandomPlaylist = IsRandomPlaylist ? 1 : 0;
            Model.Playlist.Tracks = Playlist.Select(t => t.GetModel()).ToList();
            Model.Playlist.FadeOutDuration = FadeOutDuration;
            return Model;
        }
    }
}
