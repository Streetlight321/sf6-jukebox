﻿namespace PlaylistManager.GlobalEvent
{
    public class MessageBoxEvent
    {
        public MessageBoxEvent(string message, string title)
        {
            Message = message;
            Title = title;
        }

        public string Message { get; set; }
        public string Title { get; set; }
    }
}
