﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistManager.GlobalEvent
{
    public class PlaylistValidityUpdatedEvent
    {
        public PlaylistValidityUpdatedEvent(object source)
        {
            Source = source;
        }

        public object Source { get; }
    }
}
