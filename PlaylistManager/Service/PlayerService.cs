﻿using MediaPlayerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlaylistManager.Service
{
    public class PlayerService
    {
        public event EventHandler<string> OnPlay;

        public PlayerService()
        {
            SimplePlayer = new SimplePlayer();
        }

        public void Play(string path)
        {
            if(OnPlay != null)
            {
                OnPlay(this, path);
            }

            SimplePlayer.Play(path);
        }

        public void Stop()
        {
            SimplePlayer.Stop();
        }

        SimplePlayer SimplePlayer { get; set; }
    }
}
